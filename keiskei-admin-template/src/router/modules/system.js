/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/layout'

const systemRouter = {
  path: '/security',
  component: Layout,
  redirect: '/security/sysUserList',
  name: 'Security',
  meta: { title: 'Security', icon: 'example' },
  children: [
    {
      path: '/sysUserList',
      name: 'sysUserList',
      component: () => import('@/views/security/sysUser/index'),
      meta: { title: 'Account', icon: 'table' }
    },
    {
      path: '/sysRoleList',
      name: 'sysRoleList',
      component: () => import('@/views/security/sysRole/index'),
      meta: { title: 'Role', icon: 'table' }
    },
    {
      path: '/sysDepartmentList',
      name: 'sysDepartmentList',
      component: () => import('@/views/security/sysDepartment/index'),
      meta: { title: 'Department', icon: 'tree' }
    },
    {
      path: '/sysPermissionList',
      name: 'sysPermissionList',
      component: () => import('@/views/security/sysPermission/index'),
      meta: { title: 'Permission', icon: 'tree' }
    }
  ]
}

export default systemRouter
