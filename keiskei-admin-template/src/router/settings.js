export default {
  '/views/dashboard/index': () => import('@/views/dashboard/index'),
  '/views/profile/index': () => import('@/views/profile/index'),


  // 系统设置
  '/views/security/sysUser/center': () => import('@/views/security/sysUser/center'), // 管理员
  '/views/security/sysUser/index': () => import('@/views/security/sysUser/index'), // 管理员
  '/views/security/sysRole/index': () => import('@/views/security/sysRole/index'), // 角色
  '/views/security/sysDepartment/index': () => import('@/views/security/sysDepartment/index'), // 部门
  '/views/security/sysPermission/index': () => import('@/views/security/sysPermission/index'), // 权限

}
