import { login, logout, getInfo, refuseToken } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
import { resetRouter } from '@/router'

const state = {
  id: undefined,
  token: getToken(),
  username: '',
  realName: '',
  avatar: '',
  phone: '',
  email: '',
  roles: [],
  routers: [],
  permissions: []
}

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_ID: (state, id) => {
    state.id = id
  },
  SET_REAL_NAME: (state, realName) => {
    state.realName = realName
  },
  SET_USERNAME: (state, username) => {
    state.username = username
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  },
  SET_PHONE: (state, phone) => {
    state.phone = phone
  },
  SET_EMAIL: (state, email) => {
    state.email = email
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles
  },
  SET_ROUTERS: (state, routers) => {
    state.routers = routers
  },
  SET_PERMISSIONS: (state, permissions) => {
    state.permissions = permissions
  }
}

const actions = {
  // user login
  login({ commit }, userInfo) {
    const { username, password } = userInfo
    return new Promise((resolve, reject) => {
      login({ username: username.trim(), password: password }).then(response => {
        const { data } = response
        commit('SET_TOKEN', data.token)
        setToken(data.token)
        resolve()
      }).catch(error => {
        commit('SET_USERNAME', username.trim())
        reject(error)
      })
    })
  },

  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      getInfo().then(response => {
        const { data } = response
        if (!data) {
          reject('Verification failed, please Login again.')
        }
        const { id, username, realName, avatar, phone, email, authorities, routers, permissions } = data
        commit('SET_ID', id)
        commit('SET_REAL_NAME', realName)
        commit('SET_AVATAR', avatar)
        commit('SET_PHONE', phone)
        commit('SET_EMAIL', email)
        commit('SET_USERNAME', username)
        commit('SET_ROUTERS', routers)
        commit('SET_ROLES', authorities.map(e => e.authority))
        commit('SET_PERMISSIONS', permissions)
        resolve(data)
      }).catch(error => {
        reject(error)
        logout({ commit, state })
      })
    })
  },
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      commit('SET_ID', '')
      commit('SET_TOKEN', undefined)
      commit('SET_REAL_NAME', '')
      commit('SET_AVATAR', '')
      commit('SET_PHONE', '')
      commit('SET_EMAIL', '')
      commit('SET_USERNAME', '')
      commit('SET_ROUTERS', '')
      commit('SET_ROLES', '')
      commit('SET_PERMISSIONS', '')
      removeToken()
      resetRouter()
      resolve()
    })
  },
  refuseToken({ commit, state }) {
    return new Promise((resolve, reject) => {
      refuseToken().then(response => {
        const { data } = response
        commit('SET_TOKEN', data)
        setToken(data)
        resolve(data)
      }).catch(error => {
        console.log(error)
        reject(error)
      })
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

