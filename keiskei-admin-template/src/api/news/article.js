import request from '@/utils/request'


export function getList(data) {
  return request({
    url: '/news/article/list',
    method: 'post',
    data
  })
}

export function getOptions(params) {
  return request({
    url: '/news/article/options',
    method: 'get',
    params
  })
}

export function create(data) {
  return request({
    url: '/news/article/save',
    method: 'post',
    data
  })
}

export function detail(id) {
  return request({
    url: '/news/article/detail/' + id,
    method: 'get'
  })
}

export function update(data) {
  return request({
    url: '/news/article/update',
    method: 'put',
    data
  })
}

export function updateSate(id, state) {
  return request({
    url: '/news/article/status/' + state,
    method: 'put',
    data: {
      id: id
    }
  })
}

export function deleteById(id) {
  return request({
    url: '/news/article/delete/' + id,
    method: 'delete'
  })
}

