import request from '@/utils/request'


export function getOptions(params) {
  return request({
    url: '/security/sysRole/options',
    method: 'get',
    params
  })
}

export function create(data) {
  return request({
    url: '/security/sysRole',
    method: 'post',
    data
  })
}

export function detail(id) {
  return request({
    url: '/security/sysRole/' + id,
    method: 'get'
  })
}

export function update(data) {
  return request({
    url: '/security/sysRole',
    method: 'put',
    data
  })
}

export function updateSate(id, state) {
  return request({
    url: '/security/sysRole/' + state,
    method: 'patch',
    data: {
      id: id
    }
  })
}

export function deleteById(id) {
  return request({
    url: '/security/sysRole/' + id,
    method: 'delete'
  })
}

