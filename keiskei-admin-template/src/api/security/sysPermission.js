import request from '@/utils/request'
import qs from 'qs'

export function getList(data) {
  return request({
    url: '/security/sysPermission',
    method: 'get',
    data: qs.stringify(data)
  })
}

export function getOptions(params) {
  return request({
    url: '/security/sysPermission/options',
    method: 'get',
    params
  })
}

export function create(data) {
  return request({
    url: '/security/sysPermission',
    method: 'post',
    data
  })
}

export function detail(id) {
  return request({
    url: '/security/sysPermission/' + id,
    method: 'get'
  })
}

export function update(data) {
  return request({
    url: '/security/sysPermission',
    method: 'put',
    data
  })
}

export function changeSort(data) {
  return request({
    url: '/security/sysPermission/sort',
    method: 'patch',
    data
  })
}

export function updateSate(id, state) {
  return request({
    url: '/security/sysPermission/' + state,
    method: 'patch',
    data: {
      id: id
    }
  })
}

export function deleteById(id) {
  return request({
    url: '/security/sysPermission/' + id,
    method: 'delete'
  })
}

