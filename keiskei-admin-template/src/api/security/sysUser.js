import request from '@/utils/request'

export function getOptions(params) {
  return request({
    url: '/security/sysUser/options',
    method: 'get',
    params
  })
}

export function create(data) {
  return request({
    url: '/security/sysUser',
    method: 'post',
    data
  })
}

export function detail(id) {
  return request({
    url: '/security/sysUser/' + id,
    method: 'get'
  })
}

export function update(data) {
  return request({
    url: '/security/sysUser',
    method: 'put',
    data
  })
}

export function updateSate(id, state) {
  return request({
    url: '/security/sysUser/' + state,
    method: 'patch',
    data: {
      id: id
    }
  })
}

export function deleteById(id) {
  return request({
    url: '/security/sysUser/' + id,
    method: 'delete'
  })
}

// 修改用户密码
export function updatePassword(data) {
  return request({
    url: '/security/sysUser/updatePassword',
    method: 'patch',
    data
  })
}

// 重置用户密码
export function resetPassword(data) {
  return request({
    url: '/security/sysUser/resetPassword',
    method: 'patch',
    data
  })
}

