import request from '@/utils/request'


export function getList(data) {
  return request({
    url: '/security/sysDepartment',
    method: 'get',
    data
  })
}

export function getOptions(params) {
  return request({
    url: '/security/sysDepartment/options',
    method: 'get',
    params
  })
}

export function create(data) {
  return request({
    url: '/security/sysDepartment',
    method: 'post',
    data
  })
}

export function detail(id) {
  return request({
    url: '/security/sysDepartment/' + id,
    method: 'get'
  })
}

export function update(data) {
  return request({
    url: '/security/sysDepartment',
    method: 'put',
    data
  })
}

export function updateSate(id, state) {
  return request({
    url: '/security/sysDepartment/status/' + state,
    method: 'patch',
    data: {
      id: id
    }
  })
}

export function deleteById(id) {
  return request({
    url: '/security/sysDepartment/' + id,
    method: 'delete'
  })
}

