import request from '@/utils/request'
import qs from 'qs'

// 用户登录
export function login(data) {
  return request({
    url: '/security/login',
    method: 'post',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    data: qs.stringify(data)
  })
}

// 获取用户信息
export function getInfo() {
  return request({
    url: '/security/self',
    method: 'get'
  })
}

// 退出登录
export function logout() {
  return request({
    url: '/security/logout',
    method: 'get'
  })
}
// 修改用户密码
export function update(data) {
  return request({
    url: '/security/self',
    method: 'put',
    data
  })
}

// 修改用户密码
export function updatePassword(data) {
  return request({
    url: '/security/self',
    method: 'PATCH',
    data
  })
}

// 刷新token
export function refuseToken() {
  return request({
    url: '/security/self/token',
    method: 'GET'
  })
}

