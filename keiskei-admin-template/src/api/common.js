import request from '@/utils/request'
import qs from 'qs'

export function getBaseList(url, params) {
  console.log(params)
  return request({
    url: url,
    method: 'GET',
    params
  })
}

