# convallariae
> 轻量型网站脚手架. 后台权限, 文件管理, 缓存管理, 分布式锁, 支付功能, 代码生成. 一键生成, 开箱即用

> ![](https://img.shields.io/badge/spring--boot-2.3.0-green.svg) ![](https://img.shields.io/badge/mybatis--plus-3.4-blue.svg) ![](https://img.shields.io/badge/vue--element--admin-4.00-yellow.svg)

## 项目结构

``` bash
├── convalariae                                 
|  ├── keiskei-admin-template                                后台页面
|  ├── keiskei-server-parent                                 后端服务
```
### 后端服务项目结构

``` bash
├── keiskei-server-parent                                    
|  ├── keiskei-admin-security                                管理员及鉴权
|  ├── keiskei-code-generator                                代码生成工具
|  ├── keiskei-common-core                                   基本通用工具类
|  ├── keiskei-common                                        高级通用工具类
|  ├── keiskei-payment-alipay                                支付宝支付相关
|  ├── keiskei-payment-wechat-v2                             微信v2版本支付相关
|  ├── keiskei-third-party-cache-local                       本地缓存
|  ├── keiskei-third-party-cache-redis                       redis缓存
|  ├── keiskei-third-party-cache                             缓存通用接口
|  ├── keiskei-third-party-file-alioss                       阿里对象存储
|  ├── keiskei-third-party-file-jdoss                        京东对象存储
|  ├── keiskei-third-party-file-local                        本地文件存储
|  ├── keiskei-third-party-file                              文件存储接口
|  ├── keiskei-websocket                                     websocket
```
### 代码生成工具使用
#### 示例代码
``` java
import cn.jx.cjm.codegenerator.CodeGenerator;
import cn.jx.cjm.codegenerator.base.GenerateConfig;
import java.util.LinkedHashMap;
import java.util.Map;

public class CodeGenerateUtils {
    public static void main(String[] args) throws Exception {
        Map<String, String> map = new LinkedHashMap<>();
        //    模块标识  模块名称  模块包含表
        map.put("vip,会员管理", "customer,cjm_customer_account,customer_account_detail,customer_collect");
        map.put("news,工作资讯","work,work_news,work_type");

        GenerateConfig generateConfig = new GenerateConfig();
        generateConfig.setAuthor("James Chen right_way@foxmail.com");
        generateConfig.setProjectPath("news-stand-project");
        generateConfig.setDisperseModule(false); // 每个模块是否分不同的module
        generateConfig.setMingExportNum(10);    // 导出功能最小字段数量
        generateConfig.setPermissionStartId(100); // 权限表插入数据起始id
        generateConfig.setTablePrefix(new String[]{"tb_"});  表名前缀
        generateConfig.setYmlFile("D:/demo/src/main/resources/application-dev.yml"); // yml 路径(读取数据库配置)
        generateConfig.setParentPackage("cn.jx.cjm"); // 包名

        CodeGenerator.generateFiles(generateConfig, map);
    }
}
```
#### 运行结果
1. 控制台输出

    insert into sys_permission ...... // 运行到数据库

    '/views/XXX/yyy': () => import('@/views/XXX/YYY'), ...... // 复制到/keiskei-admin-template/src/settings.js 文件中

2. resource 目录下生成 views 及 api 目录

    分别将其子内容复制到keiskei-admin-template对应目录中


### 后台页面项目结构
[请参考 vue-element-admin 文档](https://gitee.com/panjiachen/vue-element-admin?_from=gitee_search)



## 后台页面开发过程

```bash
# 修改项目配置
# /keiskei-admin-template/src/settings.js   项目基础配置
# /keiskei-admin-template/.env.development  开发环境api配置
# /keiskei-admin-template/.env.production   生产环境api配置
# /keiskei-admin-template/src/icons 添加Icon资源
# /keiskei-admin-template/public 更改 favicon.ico 资源

```

## 使用阿里普惠体

```vue
# 使用阿里字体必须先要定义，才能引用，不在定义范围中的字体无法显示字体

# 打开阿里普惠体生成页面
$ open https://www.iconfont.cn/webfont

# 将项目中所用到的文字生成所需的字体，引用线上地址
@font-face {
  font-family: 'webfont';
  font-display: swap;
  src: url('//at.alicdn.com/t/webfont_o087f99m7g.eot'); /* IE9*/
  src: url('//at.alicdn.com/t/webfont_o087f99m7g.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
  url('//at.alicdn.com/t/webfont_o087f99m7g.woff2') format('woff2'),
  url('//at.alicdn.com/t/webfont_o087f99m7g.woff') format('woff'), /* chrome、firefox */
  url('//at.alicdn.com/t/webfont_o087f99m7g.ttf') format('truetype'), /* chrome、firefox、opera、Safari, Android, iOS 4.2+*/
  url('//at.alicdn.com/t/webfont_o087f99m7g.svg#Alibaba-PuHuiTi-Regular') format('svg'); /* iOS 4.1- */
}

# 修改字体样式文件，将线上地址复制到该文件中
# /cjm-admin-template/src/styles/font.css

# 组件中使用该字体
<template>
  // ...
    <h2 style="font-family: webfont;">这里是阿里字体</h2>
  // ...
</template>

```

## 添加页面及权限

```vue
# 添加页面组件

# 添加路由配置  /keiskei-admin-template/src/router/settings.js
# Key为路径，Value为懒加载函数，将页面组件引入Vue环境
export default {
  '/views/dashboard/index': () => import('@/views/dashboard/index'),
  '/views/security/sysUser/index': () => import('@/views/system/account/index'),
  '/views/system/role/index': () => import('@/views/system/role/index'),
  '/views/system/permission/index': () => import('@/views/system/permission/index'),
  '/views/system/department/index': () => import('@/views/system/department/index')
}

# 后台配置页面  http://localhost:9528/#/security/sysPermission
*权限名称: 权限显示的名称
*权限值: 必须与路由名相同，路由跳转时需要根据该值做判断
*类型:   菜单或按钮
请求路径: 
请求方法: GET/POST/PUT/DELETE
路由名:   建议使用Vue组件中name值
URL路径:  页面地址
路由重定向:  router中redirect值
Vue资源路径: 路由配置中的Key
路由菜单Title: 菜单中显示的名称
路由菜单Icon:  菜单中显示的Icon
路由缓存: 是否缓存页面值
排序: 菜单中的排序
是否隐藏: 是否隐藏页面

```

![admin_menu_cfg](./resource/admin_menu_cfg.jpg)

## 添加按钮权限

```vue
# 页面组件中引入自定义指令
<script>
import permission from '@/directive/permission/index.js' // 权限判断指令
export default {
  // ...
  directives: { permission }
  // ...
}
</script>

# 页面模板中添加权限字段
<template>
  // ...
    <el-button v-permission="['account:add']">
      添加用户
    </el-button>
  // ...
</template>

# 后台配置按钮权限  http://localhost:9528/#/security/sysPermission
*权限名称: 权限显示的名称
*权限值: v-permission 指令中设置的值
*类型:   菜单或按钮
请求路径: 
请求方法: GET/POST/PUT/DELETE/PATCH

```

![admin_menu_cfg](./resource/admin_btn_cfg.jpg)
