package cn.jx.cjm.websocket.support;

import cn.jx.cjm.websocket.common.dto.User;

import javax.websocket.Session;

/**
 * websocket token 处理
 *
 * @author 陈加敏 right_way@foxmail.com
 * @since 2019/8/7 14:51
 */
public interface WebsocketService {

    /**
     * 验证用户信息
     *
     * @param token 用户token
     * @param authFlag 是否为管理员
     * @return .
     */
    User validate(Object token, Boolean authFlag);

    /**
     * 接收用户消息
     *
     * @param message 消息
     * @param id     发送者id
     * @param session 发送者session
     * @param authFlag 是否为管理员
     */
    void onMessage(String message, Integer id, Session session, Boolean authFlag);
}
