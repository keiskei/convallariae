package cn.jx.cjm.websocket.common.server;

import cn.jx.cjm.common.enums.BizExceptionEnum;
import cn.jx.cjm.common.exception.BizException;
import cn.jx.cjm.common.util.SpringUtils;
import cn.jx.cjm.websocket.common.configurator.GetHttpSessionConfigurator;
import cn.jx.cjm.websocket.common.constant.WebsocketConstants;
import cn.jx.cjm.websocket.common.dto.User;
import cn.jx.cjm.websocket.support.WebsocketService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @author 陈加敏 right_way@foxmail.com
 * @since 2019/8/7 14:46
 */
@Slf4j
@ServerEndpoint(value = "/{api:api|admin}/webSocket/{authFlag}", configurator = GetHttpSessionConfigurator.class)
@Component
public class WebSocketServer {


    private final WebsocketService websocketService = SpringUtils.getBean(WebsocketService.class);
    private static final CopyOnWriteArraySet<WebSocketServer> webSocketSet = new CopyOnWriteArraySet<>();
    private Integer id;
    private String name;
    private String auth = "用户端";
    private Boolean authFlag = false;
    private Session session;

    @OnOpen
    public void onOpen(Session session, EndpointConfig config, @PathParam("authFlag") String authFlag) {
        if ("admin".equals(authFlag)) {
            this.authFlag = true;
            this.auth = "管理员端";
        }
        Object token = config.getUserProperties().get(WebsocketConstants.WEBSOCKET_HEADER);
        User user = websocketService.validate(token, this.authFlag);
        if (null == user) {
            try {
                session.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            throw new BizException(BizExceptionEnum.AUTH_FORBIDDEN);
        }
        this.id = user.getId();
        this.name = user.getName();
        this.session = session;

        webSocketSet.add(this);
        log.info("{} - {} - {} 上线", this.auth, id, name);
        try {
            sendMessage("连接成功");
        } catch (IOException e) {
            log.error("webSocket IO异常");
        }
    }

    @OnClose
    public void onClose() {
        webSocketSet.remove(this);
        log.info("{} - {} - {} 下线", this.auth, id, name);
    }

    @OnMessage
    public void onMessage(String message, Session session) {
        websocketService.onMessage(message, id, session, this.authFlag);

    }

    @OnError
    public void onError(Throwable error) {
        log.error("发生错误");
        error.printStackTrace();
    }

    private void sendMessage(String message) throws IOException {
        this.session.getBasicRemote().sendText(message);
    }

    public static void sendInfo(String message, Integer id, Boolean authFlag) {
        for (WebSocketServer item : webSocketSet) {
            if (item.id.equals(id) && item.authFlag.equals(authFlag)) {
                log.info("推送消息给: {} - {} , 推送内容: {}", item.id, item.name, message);
                try {
                    item.sendMessage(message);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void sendInfo(String message, Boolean authFlag) {
        log.info("群发消息: {} - {}",authFlag,message);
        for (WebSocketServer item : webSocketSet) {
            if (item.authFlag.equals(authFlag)) {
                try {
                    item.sendMessage(message);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WebSocketServer that = (WebSocketServer) o;
        return Objects.equals(websocketService, that.websocketService) &&
                id.equals(that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(auth, that.auth) &&
                Objects.equals(authFlag, that.authFlag) &&
                session.equals(that.session);
    }

    @Override
    public int hashCode() {
        return Objects.hash(websocketService, id, name, auth, authFlag, session);
    }
}
















