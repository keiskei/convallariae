**使用说明 :**
* websocket URL 路径 /common/webSocket/{token}
* token 为用户token , 用户获取用户id

* 使用前请重写 cn.jx.cjm.websocket.token.service.impl.WebsocketTokenServiceImpl中的validate方法 , 该方法属于验证token并返回用户id


 * cn.jx.cjm.websocket.session.service.WebsocketService
 *  | -- sendMessage(String 消息 , String 用户id); 将消息推送给指定人员
 *  | -- pushMessage(String 消息); 推送消息给所有人
 *  | -- onMessage(String 消息 , String 用户id , Session 用户session); 接收用户信息 
 * 若需要对用户发来的消息做处理 , 则可重写此类中的onMessage方法
 
 ** **
 
**使用扩展 :**
* 重写 cn.jx.cjm.websocket.session.service.impl.WebsocketSessionServiceImpl 类中方法 , 可改变session的存放位置
