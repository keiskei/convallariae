package cn.jx.cjm.common.base;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 排序交换
 * </p>
 *
 * @author ：陈加敏 right_way@foxmail.com
 * @since ：2019/12/22 15:53
 */
@Data
public class BaseSortChange {
    @NotNull
    private Integer oneId;
    @NotNull
    private Integer oneSortBy;
    @NotNull
    private Integer twoId;
    @NotNull
    private Integer twoSortBy;

}
