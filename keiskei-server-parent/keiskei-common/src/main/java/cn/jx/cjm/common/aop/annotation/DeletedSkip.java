package cn.jx.cjm.common.aop.annotation;

import java.lang.annotation.*;

/**
 * 状态跳过默认
 * QueryWrapperUtils 组装查询条件时, 取消默认status=1
 * @author James Chen right_way@foxmail.com
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface DeletedSkip {

    boolean skip() default true;
}
