package cn.jx.cjm.common.config;

import cn.jx.cjm.common.aop.advice.RequestModelArgumentResolver;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * @author James Chen right_way@foxmail.com
 * @since 2020/9/29 7:54 下午
 */
@Configuration
@EnableWebMvc
public class RequestModelConfig implements WebMvcConfigurer {

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        //消息转换器与Resolver绑定
        resolvers.add(new RequestModelArgumentResolver());
    }
}
