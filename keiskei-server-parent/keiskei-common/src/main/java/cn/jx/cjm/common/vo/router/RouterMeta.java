package cn.jx.cjm.common.vo.router;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * vue 路由 meta属性
 * @author James Chen right_way@foxmail.com
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class RouterMeta {
    private String title;
    private String icon;
    private Boolean noCache;
}
