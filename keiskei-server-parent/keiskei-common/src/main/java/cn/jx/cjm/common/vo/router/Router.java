package cn.jx.cjm.common.vo.router;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

/**
 * @author James Chen right_way@foxmail.com
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Router {
    private String path;
    private String redirect;
    private String name;
    private String component;
    private Boolean hidden;


    private RouterMeta meta;
    private List<Router> children;
}
