package cn.jx.cjm.common.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 条件限定
 *
 * @author 陈加敏
 * @since 2019/7/16 9:47
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseCondition {

    /**
     * 字段名称
     */
    private String column;

    /**
     * 查询条件 > , < , >= ...
     */
    private String condition = "=";

    /**
     * 字段值
     */
    private Object value;
}
