package cn.jx.cjm.common.base;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 *
 * 
 * @author: 陈加敏
 * @since: 2019/7/12 18:58
 */
@Data
@NoArgsConstructor
public class BasePage<T> {

    /**
     * 当前页
     */
    private long current = 1;
    /**
     * 分页大小
     */
    private long size = 20L;
    /**
     * SQL 排序 ASC 集合
     */
    private List<String> ascs;
    /**
     * SQL 排序 DESC 集合
     */
    private List<String> descs;

    public BasePage(long current) {
        this.current = current;
    }
}
