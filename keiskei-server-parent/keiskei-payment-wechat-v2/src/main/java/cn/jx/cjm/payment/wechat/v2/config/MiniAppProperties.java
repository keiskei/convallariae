package cn.jx.cjm.payment.wechat.v2.config;

import lombok.Data;

/**
 * @author James Chen right_way@foxmail.com
 * @version 1.0
 * <p>
 *
 * </p>
 * @since 2020/11/4 21:08
 */
@Data
public class MiniAppProperties {

    private String appId;
    private String secret;
}
