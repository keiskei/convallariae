package cn.jx.cjm.payment.wechat.v2.dto;

import lombok.Data;

/**
 * @author James Chen right_way@foxmail.com
 * @version 1.0
 * <p>
 *
 * </p>
 * @since 2020/11/4 21:39
 */
@Data
public class AppInfo {
    private String appId;
    private String secret;
}
