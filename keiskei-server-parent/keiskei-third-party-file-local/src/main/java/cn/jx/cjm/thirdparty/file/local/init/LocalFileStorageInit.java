package cn.jx.cjm.thirdparty.file.local.init;

import cn.jx.cjm.thirdparty.file.local.config.LocalFileProperties;
import cn.jx.cjm.thirdparty.file.local.util.MultiFileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * <p>
 *
 * </p>
 *
 * @author 陈加敏 right_way@foxmail.com
 * @since 2019/11/3 0:17
 */
@Component
public class LocalFileStorageInit implements CommandLineRunner {

    @Autowired
    private LocalFileProperties localFileProperties;

    @Override
    public void run(String... args) throws Exception {
        MultiFileUtils.checkDir(localFileProperties.getPath());
        MultiFileUtils.checkDir(localFileProperties.getTempPath());
    }
}
