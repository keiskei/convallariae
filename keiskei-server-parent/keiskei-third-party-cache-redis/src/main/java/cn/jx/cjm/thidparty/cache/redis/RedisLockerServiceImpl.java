package cn.jx.cjm.thidparty.cache.redis;

import cn.jx.cjm.common.aop.annotation.LockerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;

/**
 * <p>
 * redis实现锁
 * </p>
 *
 * @author ：陈加敏 right_way@foxmail.com
 * @since ：2019/12/10 23:34
 */
@Service
@Slf4j
public class RedisLockerServiceImpl implements LockerService {

    @Autowired
    private RedissonClient redissonClient;

    @Override
    public void lock(String lockKey) {
        RLock lock = redissonClient.getLock(lockKey);
        lock.lock();
        log.info("加锁: {}", lockKey);
    }

    @Override
    public void unlock(String lockKey) {
        RLock lock = redissonClient.getLock(lockKey);
        lock.unlock();
        log.info("解锁: {}", lockKey);
    }

    @Override
    public void lock(String lockKey, TimeUnit unit, int leaseTime) {
        RLock lock = redissonClient.getLock(lockKey);
        lock.lock(leaseTime, unit);
        log.info("加锁: {}, {} ,{}", lockKey, leaseTime, unit);
    }

    @Override
    public boolean tryLock(String lockKey) {
        RLock lock = redissonClient.getLock(lockKey);
        boolean flag = lock.tryLock();
        log.info("加锁: {} , {}", lockKey, flag);
        return flag;
    }

    @Override
    public boolean tryLock(String lockKey, TimeUnit unit, long waitTime, long leaseTime) {

        RLock lock = redissonClient.getLock(lockKey);
        boolean flag = false;
        try {
            flag = lock.tryLock(waitTime, leaseTime, unit);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("加锁: {} , {} , {} ,{} , {} ", lockKey, flag, waitTime, leaseTime, unit);
        return flag;
    }

    @Override
    public boolean fairLock(String lockKey, TimeUnit unit, long waitTime, long leaseTime) {
        RLock fairLock = redissonClient.getFairLock(lockKey);

        boolean flag =false;
        try {
            flag = fairLock.tryLock(waitTime, leaseTime, unit);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("公平锁: {} , {} , {} ,{} , {} ", lockKey, flag, waitTime, leaseTime, unit);
        return flag;
    }

    @Override
    public boolean isLocked(String lockKey) {
        RLock lock = redissonClient.getLock(lockKey);
        return lock.isLocked();
    }

}
