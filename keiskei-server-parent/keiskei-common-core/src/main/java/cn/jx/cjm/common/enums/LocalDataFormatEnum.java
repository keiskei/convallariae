package cn.jx.cjm.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * <p>
 *
 * </p>
 *
 * @author ：陈加敏 right_way@foxmail.com
 * @since ：2019/12/16 23:55
 */
@Getter
@AllArgsConstructor
public enum LocalDataFormatEnum {
    //
    Y_M_D_H_M_S_S("xxxx-xx-xx xx:xx:xx.xxx","yyyy-MM-dd HH:mm:ss.SSS"),
    Y_M_D_H_M_S("xxxx-xx-xx xx:xx:xx","yyyy-MM-dd HH:mm:ss"),
    Y_M_D_H_M("xxxx-xx-xx xx:xx","yyyy-MM-dd HH:mm"),
    Y_M_D("xxxx-xx-xx","yyyy-MM-dd"),

    ;
    private final String regex;
    private final String format;


    private final static String TIME_REGEX = "[\\d]";
    private final static String TIME_PARSE = "x";

    /**
     * String 转 LocalDateTime
     * @param str .
     * @return.
     */
    public static LocalDate strToLocalDateTime(String str) {
        String strRegex = str.replaceAll(TIME_REGEX,TIME_PARSE);
        for (LocalDataFormatEnum formatEnum: LocalDataFormatEnum.values()) {
            if (formatEnum.getRegex().equals(strRegex)) {
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern(formatEnum.getFormat());
                return LocalDate.parse(str,dtf);
            }
        }
        return null;
    }

}
