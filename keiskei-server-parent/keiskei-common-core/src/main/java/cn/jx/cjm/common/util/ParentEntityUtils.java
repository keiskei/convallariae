package cn.jx.cjm.common.util;

import cn.jx.cjm.common.base.ParentEntity;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * @author 陈加敏
 */
public class ParentEntityUtils<T extends ParentEntity<T>> {


    private final Map<Integer, List<T>> map;

    public ParentEntityUtils(List<T> list) {
        map = new LinkedHashMap<>();

        for (T t : list) {
            if (map.containsKey(t.getParentId())) {
                map.get(t.getParentId()).add(t);
            } else {
                List<T> tList = new ArrayList<>();
                tList.add(t);
                map.put(t.getParentId(), tList);
            }
        }
    }

    public List<T> getTree(Integer id) {
        List<T> tempList = map.get(id);
        if (CollectionUtils.isEmpty(tempList)) {
            return null;
        }
        List<T> responses = new ArrayList<>(tempList.size());
        for (T t : tempList) {
            t.setChildren(getTree(t.getId()));
            responses.add(t);
        }
        return responses;
    }

    public Set<Integer> getChildIds(Integer id) {
        List<T> tempList = map.get(id);
        Set<Integer> tempIds = new HashSet<>();
        if (CollectionUtils.isEmpty(tempList)) {
            return tempIds;
        }
        for (T t : tempList) {
            tempIds.add(t.getId());
            tempIds.addAll(getChildIds(t.getId()));
        }
        return tempIds;
    }

}
