package cn.jx.cjm.common.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * yes or no
 * @author James Chen right_way@foxmail.com
 * @since 2018年9月30日 下午5:12:51
 */
@Getter
@AllArgsConstructor
public enum YesOrNoEnum {
	//

	YES(1, "是"),
	NO(2,"否"),
	;

	public static String get(Integer code) {
		for (YesOrNoEnum yesOrNoEnum : YesOrNoEnum.values()) {
			if (yesOrNoEnum.getCode().equals(code)) {
				return yesOrNoEnum.getMessage();
			}
		}
		return "";
	}

    private final Integer code;
    private final String message;

}
