package cn.jx.cjm.common.util;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * 时间工具类
 *
 * @author James Chen right_way@foxmail.com
 * @since 2018年9月30日 下午2:38:23
 */
public class DateTimeUtils {

    /**
     * 标准日期格式
     */
    public final static String NORM_DATE_PATTERN = "yyyy-MM-dd";
    /**
     * 标准时间格式  HH:mm:ss
     */
    public final static String NORM_TIME_PATTERN = "HH:mm:ss";
    /**
     * 标准日期时间格式，精确到分
     */
    public final static String NORM_DATETIME_MINUTE_PATTERN = "yyyy-MM-dd HH:mm";
    /**
     * 标准日期时间格式，精确到秒
     */
    public final static String NORM_DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    /**
     * 标准日期时间格式，精确到毫秒
     */
    public final static String NORM_DATETIME_MS_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS";
    /**
     * HTTP头中日期时间格式
     */
    public final static String HTTP_DATETIME_PATTERN = "EEE, dd MMM yyyy HH:mm:ss z";
    /**
     * 十二位时间戳 yyyyMMddHHmmss
     */
    public final static String NORM_DATETIME_STAMP_PATTERN = "yyyyMMddHHmm";
    /**
     * 十5位时间戳 yyyyMMddHHmmss
     */
    public final static String NORM_DATETIME_STAMP_MS_PATTERN = "yyyyMMddHHmmSSS";
    /**
     * 标准日期（不含时间）格式化器
     */
    private static ThreadLocal<SimpleDateFormat> NORM_DATE_FORMAT = new ThreadLocal<SimpleDateFormat>() {
        @Override
        synchronized protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat(NORM_DATE_PATTERN);
        }
    };
    /**
     * 标准时间格式化器
     */
    private static ThreadLocal<SimpleDateFormat> NORM_TIME_FORMAT = new ThreadLocal<SimpleDateFormat>() {
        @Override
        synchronized protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat(NORM_TIME_PATTERN);
        }
    };
    /**
     * 标准日期时间格式化器
     */
    private static ThreadLocal<SimpleDateFormat> NORM_DATETIME_FORMAT = new ThreadLocal<SimpleDateFormat>() {
        @Override
        synchronized protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat(NORM_DATETIME_PATTERN);
        }
    };
    /**
     * HTTP日期时间格式化器
     */
    private static ThreadLocal<SimpleDateFormat> HTTP_DATETIME_FORMAT = new ThreadLocal<SimpleDateFormat>() {
        @Override
        synchronized protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat(HTTP_DATETIME_PATTERN, Locale.US);
        }
    };

    /**
     * 字符串时间格式转LocalDateTime
     *
     * @param date      时间
     * @param formatter 格式
     * @return 。
     */
    public static LocalDateTime strToLocalDate(String date, String formatter) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern(formatter);
        LocalDateTime ldt;
        try {
            ldt = LocalDateTime.parse(date, df);
            return ldt;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static LocalDateTime strToLocalDate(String time) {
        return strToLocalDate(time, NORM_DATETIME_PATTERN);
    }

    /**
     * LocalDate转字符串时间格式
     *
     * @param date 时间
     * @return yyyy-MM-dd HH:mm:ss
     */
    public static String localDateToStr(LocalDateTime date) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return df.format(date);
    }


    /**
     * LocalDateTime 格式化
     *
     * @param time      时间
     * @param formatter 格式
     * @return .
     */
    public static String localDateTimeFormate(LocalDateTime time, String formatter) {
        DateTimeFormatter sdf = DateTimeFormatter.ofPattern(formatter);
        return sdf.format(time);

    }
}
