package cn.jx.cjm.common.base;

import lombok.Data;

import java.io.Serializable;

/**
 * 权限用户
 *
 * @author James Chen right_way@foxmail.com
 * @since 2018年10月11日 下午4:19:31
 */
@Data
public class MDCUser implements Serializable {
    private static final long serialVersionUID = 3149333856334677639L;

    private Integer id;
    private String ip;
    private String realName;
    private String name;
}
