package cn.jx.cjm.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * <p>
 *
 * </p>
 *
 * @author ：陈加敏 right_way@foxmail.com
 * @since ：2019/12/16 23:55
 */
@Getter
@AllArgsConstructor
public enum LocalTimeFormatEnum {
    //
    Y_M_D_H_M_S_S("xxxx-xx-xx xx:xx:xx.xxx","yyyy-MM-dd HH:mm:ss.SSS"),
    Y_M_D_H_M_S("xxxx-xx-xx xx:xx:xx","yyyy-MM-dd HH:mm:ss"),
    Y_M_D_H_M("xxxx-xx-xx xx:xx","yyyy-MM-dd HH:mm"),
    H_M_S_S("xx:xx:xx.xxx","HH:mm:ss.SSS"),
    H_M_S("xx:xx:xx","HH:mm:ss"),
    H_M("xx:xx","HH:mm"),

    ;
    private final String regex;
    private final String format;


    private final static String TIME_REGEX = "[\\d]";
    private final static String TIME_PARSE = "x";

    /**
     * String 转 LocalDateTime
     * @param str .
     * @return .
     */
    public static LocalTime strToLocalDateTime(String str) {
        String strRegex = str.replaceAll(TIME_REGEX,TIME_PARSE);
        for (LocalTimeFormatEnum formatEnum: LocalTimeFormatEnum.values()) {
            if (formatEnum.getRegex().equals(strRegex)) {
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern(formatEnum.getFormat());
                return LocalTime.parse(str,dtf);
            }
        }
        return null;
    }

    public static void main(String[] args) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(Y_M_D_H_M.getFormat());
        System.out.println(LocalTime.parse("2019-12-07 12:12",dtf));
    }

}
