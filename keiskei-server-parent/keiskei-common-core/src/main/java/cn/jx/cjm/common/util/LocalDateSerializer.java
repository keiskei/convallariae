package cn.jx.cjm.common.util;


import cn.jx.cjm.common.enums.LocalDataFormatEnum;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * localDataTime序列化工具
 *
 * @author 陈加敏 right_way@foxmail.com
 * @since 2019/8/5 20:45
 */
public class LocalDateSerializer extends JsonSerializer<LocalDate> {

    @Override
    public void serialize(LocalDate localDate, JsonGenerator jsonGenerator,
                          SerializerProvider serializerProvider) throws IOException {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(LocalDataFormatEnum.Y_M_D.getFormat());
        jsonGenerator.writeString(localDate.format(dtf));
    }
}
