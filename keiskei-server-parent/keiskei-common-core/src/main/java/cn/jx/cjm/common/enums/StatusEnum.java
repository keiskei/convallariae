package cn.jx.cjm.common.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 状态枚举
 * @author James Chen right_way@foxmail.com
 * @since 2018年9月30日 下午5:12:51
 */
@Getter
@AllArgsConstructor
public enum StatusEnum {
	//

	NORMAL(1, "启用"),
	DISABLE(2, "禁用"),
	DELETED(3, "删除"),
	;

	public static String get(Integer code) {
		for (StatusEnum statusEnum : StatusEnum.values()) {
			if (statusEnum.getCode().equals(code)) {
				return statusEnum.getMessage();
			}
		}
		return "";
	}

    private final Integer code;
    private final String message;
}
