package cn.jx.cjm.common.base;

import java.util.List;

/**
 * 树状实体类统一接口
 *
 * @author 陈加敏
 */
public interface ParentEntity<T> {

    /**
     * 子类ID
     * @return
     */
    Integer getId();

    /**
     * 子类ID
     * @param id
     */
    void setId(Integer id);

    /**
     * 父类ID
     * @return
     */
    Integer getParentId();

    /**
     * 父类ID
     * @param parentId
     */
    void setParentId(Integer parentId);

    /**
     * 子类集合
     * @return
     */
    List<T> getChildren();

    /**
     * 子类集合
     * @param children
     */
    void setChildren(List<T> children);
}
