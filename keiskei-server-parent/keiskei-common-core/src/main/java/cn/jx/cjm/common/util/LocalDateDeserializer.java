package cn.jx.cjm.common.util;

import cn.jx.cjm.common.enums.LocalDataFormatEnum;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.time.LocalDate;

/**
 * localDateTime反序列化工具
 *
 * @author 陈加敏 right_way@foxmail.com
 * @since 2019/8/5 20:50
 */
public class LocalDateDeserializer extends JsonDeserializer<LocalDate> {

    @Override
    public LocalDate deserialize(JsonParser jp, DeserializationContext deserializationContext) throws IOException {
        return LocalDataFormatEnum.strToLocalDateTime(jp.getText());
    }
}
