package cn.jx.cjm.common.util;

import cn.jx.cjm.common.enums.LocalTimeFormatEnum;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.time.LocalTime;

/**
 * localDateTime反序列化工具
 *
 * @author 陈加敏 right_way@foxmail.com
 * @since 2019/8/5 20:50
 */
public class LocalTimeDeserializer extends JsonDeserializer<LocalTime> {

    @Override
    public LocalTime deserialize(JsonParser jp, DeserializationContext deserializationContext) throws IOException {
        return LocalTimeFormatEnum.strToLocalDateTime(jp.getText());
    }
}
