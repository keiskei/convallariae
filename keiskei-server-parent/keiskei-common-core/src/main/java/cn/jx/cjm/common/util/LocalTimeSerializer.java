package cn.jx.cjm.common.util;


import cn.jx.cjm.common.enums.LocalTimeFormatEnum;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * localDataTime序列化工具
 *
 * @author 陈加敏 right_way@foxmail.com
 * @since 2019/8/5 20:45
 */
public class LocalTimeSerializer extends JsonSerializer<LocalTime> {

    @Override
    public void serialize(LocalTime localDateTime, JsonGenerator jsonGenerator,
                          SerializerProvider serializerProvider) throws IOException {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(LocalTimeFormatEnum.H_M_S_S.getFormat());
        jsonGenerator.writeString(localDateTime.format(dtf));
    }
}
