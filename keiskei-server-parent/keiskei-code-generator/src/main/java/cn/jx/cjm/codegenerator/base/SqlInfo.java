package cn.jx.cjm.codegenerator.base;

import lombok.Data;

/**
 * @author James Chen right_way@foxmail.com
 * @since 2020/6/3 15:21
 */
@Data
public class SqlInfo {
    private String sqlUrl;
    private String sqlDriverName;
    private String sqlUserName;
    private String sqlPassword;
}
