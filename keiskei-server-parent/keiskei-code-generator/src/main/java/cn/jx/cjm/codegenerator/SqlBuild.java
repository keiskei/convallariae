package cn.jx.cjm.codegenerator;

import cn.jx.cjm.common.util.DateTimeUtils;
import com.baomidou.mybatisplus.generator.config.po.TableField;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cn.jx.cjm.codegenerator.CodeGenerator.processEntityName;

/**
 * @author James Chen right_way@foxmail.com
 * @since 2020/6/2 19:27
 */
public class SqlBuild {
    public static int moduleStartId = 1;

    public static Map<String, String> tableModuleMap = new HashMap<>();
    public static String NOW = DateTimeUtils.localDateToStr(LocalDateTime.now());
    public static List<String> routers = new ArrayList<>();
    public static List<String> permissionSql = new ArrayList<>();
    public static final String PERMISSION_SQL = "INSERT INTO `sys_permission` " +
            "(id,parent_id,authority,authority_name,authority_path,authority_method,router_path,router_name,router_redirect,router_component,router_title,router_icon,router_no_cache,sort_by,type,hidden,create_time,update_time) " +
            "VALUES " +
            "(%s, %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);";

    public static void buildSql(int pageSort, int parentId, String moduleName, TableInfo tableInfoNow) {
        int minExport = CodeGenerator.generateConfig.getMingExportNum();
        boolean hasStatus = false;
        boolean hasChild = false;
        boolean hasSort = false;
        for (TableField tableField : tableInfoNow.getFields()) {
            if ("status".equals(tableField.getPropertyName())) {
                hasStatus = true;
            }
            if ("parentId".equals(tableField.getPropertyName())) {
                hasChild = true;
            }
            if ("sortBy".equals(tableField.getPropertyName())) {
                hasSort = true;
            }
        }

        routers.add("  '/views/" + moduleName + "/" + CodeGenerator.processEntityName(tableInfoNow.getEntityName()) + "/index': () => import('@/views/" + moduleName + "/" + CodeGenerator.processEntityName(tableInfoNow.getEntityName()) + "/index'), // " + tableInfoNow.getComment());
        int buttonStartId = moduleStartId++;
        permissionSql.add(getPermissionSql(buttonStartId, parentId, moduleName + ":" + CodeGenerator.processEntityName(tableInfoNow.getEntityName()), tableInfoNow.getComment(), "/admin/" + moduleName + "/" + CodeGenerator.processEntityName(tableInfoNow.getEntityName()), "GET", "/" + moduleName + "/" + CodeGenerator.processEntityName(tableInfoNow.getEntityName()), CodeGenerator.processEntityName(tableInfoNow.getEntityName()), null, "/views/" + moduleName + "/" + CodeGenerator.processEntityName(tableInfoNow.getEntityName()) + "/index", "tree", pageSort, "menu", 0));

        int i = 1;
        permissionSql.add(getPermissionSql(moduleStartId++, buttonStartId, moduleName + ":" + CodeGenerator.processEntityName(tableInfoNow.getEntityName()) + ":add", "添加", "/admin/" + moduleName + "/" + CodeGenerator.processEntityName(tableInfoNow.getEntityName()), "POST", null, null, null, null, null, i++, "button", 0));
        permissionSql.add(getPermissionSql(moduleStartId++, buttonStartId, moduleName + ":" + CodeGenerator.processEntityName(tableInfoNow.getEntityName()) + ":edit", "编辑", "/admin/" + moduleName + "/" + CodeGenerator.processEntityName(tableInfoNow.getEntityName()), "PUT", null, null, null, null, null, i++, "button", 0));
        permissionSql.add(getPermissionSql(moduleStartId++, buttonStartId, moduleName + ":" + CodeGenerator.processEntityName(tableInfoNow.getEntityName()) + ":delete", "删除", "/admin/" + moduleName + "/" + CodeGenerator.processEntityName(tableInfoNow.getEntityName()) + "/{spring:[0-9]+}", "DELETE", null, null, null, null, null, i++, "button", 0));
        permissionSql.add(getPermissionSql(moduleStartId++, buttonStartId, moduleName + ":" + CodeGenerator.processEntityName(tableInfoNow.getEntityName()) + ":detail", "详情", "/admin/" + moduleName + "/" + CodeGenerator.processEntityName(tableInfoNow.getEntityName()) + "/{spring:[0-9]+}", "GET", null, null, null, null, null, i++, "button", 0));


        if (hasStatus) {
            permissionSql.add(getPermissionSql(moduleStartId++, buttonStartId, moduleName + ":" + CodeGenerator.processEntityName(tableInfoNow.getEntityName()) + ":enable", "启用", "/admin/" + moduleName + "/" + CodeGenerator.processEntityName(tableInfoNow.getEntityName()) + "/status/1", "PATCH", null, null, null, null, null, i++, "button", 0));
            permissionSql.add(getPermissionSql(moduleStartId++, buttonStartId, moduleName + ":" + CodeGenerator.processEntityName(tableInfoNow.getEntityName()) + ":disable", "禁用", "/admin/" + moduleName + "/" + CodeGenerator.processEntityName(tableInfoNow.getEntityName()) + "/status/2", "PATCH", null, null, null, null, null, i++, "button", 0));
        }
        if (!hasChild && tableInfoNow.getFields().size() > minExport) {
            permissionSql.add(getPermissionSql(moduleStartId++, buttonStartId, moduleName + ":" + CodeGenerator.processEntityName(tableInfoNow.getEntityName()) + ":export", "导出", "/admin/" + moduleName + "/" + CodeGenerator.processEntityName(tableInfoNow.getEntityName()) + "/export", "GET", null, null, null, null, null, i++, "button", 0));
            permissionSql.add(getPermissionSql(moduleStartId++, buttonStartId, moduleName + ":" + CodeGenerator.processEntityName(tableInfoNow.getEntityName()) + ":import", "导入", "/admin/" + moduleName + "/" + CodeGenerator.processEntityName(tableInfoNow.getEntityName()) + "/import", "POST", null, null, null, null, null, i++, "button", 0));
        }
        if (hasSort) {
            permissionSql.add(getPermissionSql(moduleStartId++, buttonStartId, moduleName + ":" + CodeGenerator.processEntityName(tableInfoNow.getEntityName()) + ":sort", "排序", "/admin/" + moduleName + "/" + CodeGenerator.processEntityName(tableInfoNow.getEntityName()) + "/sort", "PATCH", null, null, null, null, null, i++, "button", 0));
        }
        for (TableField tableField : tableInfoNow.getFields()) {
            if (tableField.getPropertyName().endsWith("Id") && !"parentId".equals(tableField.getPropertyName()) && tableField.getPropertyType().equals("Integer")) {
                permissionSql.add(getPermissionSql(moduleStartId++, buttonStartId, tableModuleMap.get(tableField.getPropertyName().replace("Id", "")) + ":" + tableField.getPropertyName().replace("Id", "") + ":options", tableField.getComment().replace("id", "").replace("Id", "").replace("ID", "") + "下拉框", "/admin/" + tableModuleMap.getOrDefault(CodeGenerator.processEntityName(tableField.getPropertyName()).replace("Id", ""), "undefind") + "/" + CodeGenerator.processEntityName(tableField.getPropertyName()).replace("Id", "") + "/options", "GET", null, null, null, null, null, i++, "button", 0));
            }
        }
        permissionSql.add("");
    }

    public static String getPermissionSql(Integer id, Integer parentId, String authority, String authorityName, String authorityPath, String authorityMethod, String routerPath, String routerName, String routerRedirect, String routerComponent, String routerIcon, Integer sort, String type, Integer hidden) {
        return String.format(PERMISSION_SQL,
                id,
                null == parentId ? "NULL" : "'" + parentId + "'",
                null == authority ? "NULL" : "'" + authority + "'",
                null == authorityName ? "NULL" : "'" + authorityName + "'",
                null == authorityPath ? "NULL" : "'" + authorityPath + "'",
                null == authorityMethod ? "NULL" : "'" + authorityMethod + "'",
                null == routerPath ? "NULL" : "'" + routerPath + "'",
                null == routerName ? "NULL" : "'" + routerName + "'",
                null == routerRedirect ? "NULL" : "'" + routerRedirect + "'",
                null == routerComponent ? "NULL" : "'" + routerComponent + "'",
                null == authorityName ? "NULL" : "'" + authorityName + "'",
                null == routerIcon ? "NULL" : "'" + routerIcon + "'",
                0,
                sort,
                "'" + type + "'",
                hidden,
                "'" + NOW + "'",
                "'" + NOW + "'");

    }

    public static void printSql() {
        for (String router : routers) {
            System.out.println(router);
        }
        System.out.println();
        for (String sql : permissionSql) {
            System.out.println(sql);
        }

    }
}
