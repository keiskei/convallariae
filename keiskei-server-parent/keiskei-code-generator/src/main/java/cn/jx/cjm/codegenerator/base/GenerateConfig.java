package cn.jx.cjm.codegenerator.base;

import lombok.Data;

/**
 * @author James Chen right_way@foxmail.com
 * @since 2020/6/3 10:12
 */
@Data
public class GenerateConfig {

    /**
     * 基础包名
     */
    private String parentPackage;
    /**
     * 指定yml文件
     */
    private String ymlFile = "application-dev.yml";
    /**
     * 表名前缀
     */
    private String[] tablePrefix;
    /**
     * 是否分开MAVEN模块
     */
    private Boolean disperseModule = false;
    /**
     * 默认模块路径
     */
    private String projectPath = "keiskei-project";
    /**
     * 作者
     */
    private String author = " .";
    /**
     * 权限表起始ID
     */
    private Integer permissionStartId = 100;
    /**
     * 导出功能字段最小数量
     */
    private Integer mingExportNum = 10;

    /**
     * yml数据库前缀
     */
    private String sqlYmlPrefix = "spring.datasource";

    private String supperEntity = "cn.jx.cjm.common.base.BaseEntity";

    private String[] superEntityColumns = {"id", "create_time", "update_time", "deleted"};

    private String idType = "Integer";

}
