<#assign hasStatus=false>
<#assign hasChild=false>
<#assign hasSort=false>
<#list table.fields as field>
<#if field.propertyName=="status"><#assign hasStatus=true></#if>
<#if field.propertyName=="parentId"><#assign hasChild=true></#if>
<#if field.propertyName=="sortBy"><#assign hasSort=true></#if>
</#list>
package ${package.Controller};

import cn.jx.cjm.common.aop.annotation.RequestModel;
import cn.jx.cjm.${cfg.cjmModuleName}.dto.request.${entity}Request;
import cn.jx.cjm.${cfg.cjmModuleName}.dto.response.${entity}Response;
import ${package.Entity}.${entity};
<#if hasSort>
import cn.jx.cjm.common.base.BaseSortChange;
</#if>
import cn.jx.cjm.common.base.BaseRequest;
import com.baomidou.mybatisplus.extension.api.R;
import cn.jx.cjm.common.util.ExcelUtils;
import com.baomidou.mybatisplus.core.metadata.IPage;
import cn.jx.cjm.${cfg.cjmModuleName}.support.${entity}SupportService;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.SelectKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.util.List;

<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>

/**
 * ${table.comment!} 前端控制器
 *
 * @author ${author}
 * @since ${date}
 */

@Api(tags = "后台 - ${table.comment!}")
@RestController
@RequestMapping("/admin/${cfg.cjmModuleName}/${table.entityPath?uncap_first}")
public class ${entity}Controller {

    @Autowired
    private ${entity}SupportService ${entity?uncap_first}SupportService;

    @ApiOperation("${table.comment}列表")
    @GetMapping
    public R<<#if hasChild>List<#else>IPage</#if><${entity}Response>> list(<#if hasChild>${entity}Request request<#else>@RequestModel BaseRequest<${entity}> request</#if>) {
        return R.ok(${entity?uncap_first}SupportService.list(request));
    }

<#if table.fields!?size gt cfg.minExport && !hasChild>
    @ApiOperation("${table.comment}导出")
    @GetMapping("/export")
    public void export(@RequestModel BaseRequest<${entity}> request,HttpServletResponse response) {
        List<${entity}Response> list = ${entity?uncap_first}SupportService.export(request);
        ExcelUtils.createExcel(response,${entity}Response.dataList(list));
    }

    @ApiOperation("${table.comment}导入")
    @PostMapping("/import")
    public R<Boolean> importData(MultipartFile file) throws Exception{
        ${entity?uncap_first}SupportService.importData(ExcelUtils.readExcel(file.getInputStream(), 0 ,0));
        return R.ok(true);
    }

</#if>
    @ApiOperation("${table.comment}下拉框")
    @GetMapping("/options")
    public R<List<${entity}Response>> options(<#if hasChild>@RequestParam(required = false) ${cfg.idType} parentId</#if>){
        return R.ok(${entity?uncap_first}SupportService.options(<#if hasChild>parentId</#if>));
    }

    @ApiOperation("${table.comment}详情")
    @GetMapping("/{id}")
    public R<${entity}Response> getOne(
        @NotNull(message = "请选择${table.comment}") @PathVariable("id") ${cfg.idType} id) {
        return R.ok(${entity?uncap_first}SupportService.getOne(id));
    }

    @ApiOperation("${table.comment}保存")
    @PostMapping
    public R<Boolean> save(@RequestBody @Validated({Insert.class}) ${entity}Request request) {
        ${entity?uncap_first}SupportService.save(request);
        return R.ok(true);
    }

    @ApiOperation("${table.comment}更新")
    @PutMapping
    public R<Boolean> update(@RequestBody @Validated({Update.class}) ${entity}Request request) {
        ${entity?uncap_first}SupportService.update(request);
        return R.ok(true);
    }

<#if hasSort>
    @ApiOperation("${table.comment}调序")
    @PatchMapping("/sort")
    public R<Boolean> update(@RequestBody @Validated BaseSortChange request) {
        ${entity?uncap_first}SupportService.sort(request);
        return R.ok(true);
    }

</#if>
<#if hasStatus>
    @ApiOperation("${table.comment}更新状态")
    @PatchMapping("/status/{status}")
    public R<Boolean> update(
            @PathVariable("status") @NotNull Integer status,
            @RequestBody @Validated({SelectKey.class}) ${entity}Request request) {
        ${entity?uncap_first}SupportService.status(request.getId(), status);
        return R.ok(true);
    }

</#if>
    @ApiOperation("${table.comment}删除")
    @DeleteMapping("/{id}")
    public R<Boolean> remove(
        @NotNull(message = "请选择${table.comment}") @PathVariable("id") ${cfg.idType} id) {
        ${entity?uncap_first}SupportService.remove(id);
        return R.ok(true);
    }
}