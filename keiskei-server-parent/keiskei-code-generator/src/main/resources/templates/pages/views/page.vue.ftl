<template>
  <div class="app-container">
    <div class="filter-container">
      <el-row :gutter="10">
        <el-col :xs="24" :sm="24" :md="12" :lg="16" :xl="16">
<#assign hasStatus=false>
<#assign hasImage=false>
<#assign hasVideo=false>
<#assign hasChild=false>
<#assign hasLevel=false>
<#assign hasOptions=false>
<#assign hasSort=false>
<#assign hasContent=false>
<#list table.fields as field>
  <#if field.propertyName=="status"><#assign hasStatus=true></#if>
  <#if field.propertyName=="level"><#assign hasLevel=true></#if>
  <#if field.propertyName=="parentId"><#assign hasChild=true></#if>
  <#if field.propertyName=="sortBy"><#assign hasSort=true></#if>
  <#if field.propertyName?lower_case?ends_with("image")><#assign hasImage=true></#if>
  <#if field.propertyName?lower_case?ends_with("video")><#assign hasVideo=true></#if>
  <#if field.propertyName?lower_case?ends_with("content")><#assign hasContent=true></#if>
</#list>
<#if hasChild>
  <#list table.fields as field>
    <#if field.propertyName=="level" || field.propertyName?lower_case?ends_with("image") || field.propertyName?lower_case?ends_with("video") || field.propertyName?lower_case?ends_with("describe") || field.propertyName?lower_case?ends_with("content")>
    <#elseif field.propertyName?ends_with("Id") && field.propertyName != "parentId" && field.propertyType == "Integer">
      <#assign hasOptions=true>
      <#assign optionsKey=field.propertyName?remove_ending("Id")?uncap_first/>
        <el-select v-model="listQuery.${field.propertyName}" clearable class="filter-item" v-permission="['${cfg.tableModuleMap[optionsKey]!''}:${table.fields[i].propertyName?remove_ending("Id")}:options']" placeholder="${field.comment}" filterable @change="fetchListData">
          <el-option v-for="item in ${field.propertyName?remove_ending("Id")}Options" :key="item.id" :label="item.name" :value="item.id"></el-option>
        </el-select>
    <#elseif field.propertyName == "status">
        <el-select v-model="listQuery.${field.propertyName}" clearable class="filter-item" placeholder="${field.comment}" filterable @change="fetchListData">
          <el-option v-for="item in statusOptions" :key="item.id" :label="item.name" :value="item.id"></el-option>
        </el-select>
    <#else>
        <el-input v-model="listQuery.${field.propertyName}" clearable class="filter-item" placeholder="${field.comment}" @keyup.enter.native="fetchListData"></el-input>
    </#if>
  </#list>
<#else>
  <#list 0..(table.fields!?size-1) as i>
    <#if table.fields[i].keyFlag>
    <#elseif table.fields[i].propertyName=="level" || table.fields[i].propertyName?lower_case?ends_with("describe") || table.fields[i].propertyName?lower_case?ends_with("content") || table.fields[i].propertyName?lower_case?contains("image") || table.fields[i].propertyName?lower_case?contains("video")>
    <#elseif table.fields[i].propertyName == "status">
          <el-select v-model="listQuery.${table.fields[i].propertyName}" clearable class="filter-item" placeholder="${table.fields[i].comment}" filterable @change="fetchListData">
            <el-option v-for="item in statusOptions" :key="item.id" :label="item.name" :value="item.id"></el-option>
          </el-select>
    <#elseif table.fields[i].propertyName?ends_with("Id")  && table.fields[i].propertyName != "parentId" && table.fields[i].propertyType == "Integer">
      <#assign hasOptions=true>
      <#assign optionsKey=table.fields[i].propertyName?remove_ending("Id")?uncap_first/>
          <el-select v-model="listQuery[${i}].value" clearable class="filter-item" v-permission="['${cfg.tableModuleMap[optionsKey]!''}:${table.fields[i].propertyName?remove_ending("Id")}:options']" placeholder="${table.fields[i].comment}" filterable @change="fetchListData">
            <el-option v-for="item in ${table.fields[i].propertyName?remove_ending("Id")}Options" :key="item.id" :label="item.name" :value="item.id"></el-option>
          </el-select>
    <#elseif table.fields[i].propertyType == "LocalDateTime">
          <el-date-picker v-model="listQuery[${i}].value" clearable class="filter-item-time" format="yyyy-MM-dd" value-format="yyyy-MM-dd HH:mm:ss.SSS" @change="fetchListData" type="daterange" :default-time="['00:00:00.000', '23:59:59.999']" range-separator=" - " start-placeholder="${table.fields[i].comment}开始" end-placeholder="${table.fields[i].comment}结束"></el-date-picker>
    <#elseif table.fields[i].propertyType == "BigDecimal">
    <#elseif table.fields[i].propertyType == "String">
          <el-input v-model="listQuery[${i}].value" clearable class="filter-item" placeholder="${table.fields[i].comment}" @keyup.enter.native="fetchListData"></el-input>
    </#if>
  </#list>
  <#if superEntityClass??>
          <el-date-picker v-model="listQuery[${table.fields!?size}].value" clearable class="filter-item-time" format="yyyy-MM-dd" value-format="yyyy-MM-dd HH:mm:ss.SSS" @change="fetchListData" type="daterange" :default-time="['00:00:00.000', '23:59:59.999']" range-separator=" - " start-placeholder="创建开始" end-placeholder="创建结束"></el-date-picker>
  </#if>
</#if>
        </el-col>
        <el-col :xs="24" :sm="24" :md="12" :lg="8" :xl="8" style="text-align: right">
          <el-button v-waves class="filter-button" icon="el-icon-back" @click="$router.back()">返回</el-button>
          <el-button v-waves type="primary" class="filter-button" icon="el-icon-search" plain @click="fetchListData">搜索</el-button>
          <el-button v-waves type="primary" class="filter-button" icon="el-icon-edit" v-permission="['${cfg.cjmModuleName}:${entity?uncap_first}:add']" @click="handleCreate${entity}<#if hasChild>(listQuery.parentId)</#if>">新建</el-button>
<#if !hasChild>
  <#if table.fields!?size gt cfg.minExport>
          <el-button v-waves type="success" class="filter-button" icon="el-icon-download" v-permission="['${cfg.cjmModuleName}:${entity?uncap_first}:export']" @click="handleExport${entity}">导出</el-button>
          <el-popover placement="right" width="400" trigger="click" v-permission="['${cfg.cjmModuleName}:${entity?uncap_first}:import']">
            <el-upload class="upload-demo" :action="requestData.requestUrl" :headers="requestData.headerToken" accept=".xls" :on-success="handleImportSuccess" :on-error="handleImportFail" :drag="true">
              <i class="el-icon-upload"></i>
              <div class="el-upload__text">将文件拖到此处，或<em>点击上传</em></div>
            </el-upload>
            <el-button v-waves class="filter-button" icon="el-icon-upload2" type="success" plain slot="reference">导入</el-button>
          </el-popover>
  </#if>
</#if>
        </el-col>
      </el-row>
    </div>
    <div class="table-container">
      <#if hasChild><tree-table-list<#else><base-list</#if> ref="table" url="/${cfg.cjmModuleName}/${entity?uncap_first}" :query="listQuery" :columns="columns" :format="format"<#if hasSort> @indexChange="handleChangeSortBy"</#if>>
<#if hasStatus>
        <template slot="status" slot-scope="scope">
          <el-tag v-if="scope.row.status === 1" type="success">启用</el-tag>
          <el-tag v-if="scope.row.status === 2" type="warning">禁用</el-tag>
        </template>
</#if>
        <template slot="actions" slot-scope="scope">
<#if hasChild>
          <el-button v-waves v-permission="['${cfg.cjmModuleName}:${entity?uncap_first}:edit']" type="primary" @click="handleCreate${entity}(scope.row.id<#if hasLevel>, scope.row.level+1</#if>)">新建子类</el-button>
</#if>
<#if table.fields!?size gt cfg.minExport>
          <el-button v-waves type="primary" plain @click="handleDetail${entity}(scope.$index, scope.row)">详情</el-button>
</#if>
          <el-button v-waves v-permission="['${cfg.cjmModuleName}:${entity?uncap_first}:edit']" type="primary" @click="handleEdit${entity}(scope.$index, scope.row)">编辑</el-button>
<#if hasStatus>
          <el-button v-waves v-if="scope.row.status === 1" v-permission="['${cfg.cjmModuleName}:${entity?uncap_first}:disable']" type="warning" @click="handleStatus${entity}(scope.row.id, 2)">禁用</el-button>
          <el-button v-waves v-if="scope.row.status === 2" v-permission="['${cfg.cjmModuleName}:${entity?uncap_first}:enable']" type="success" @click="handleStatus${entity}(scope.row.id, 1)">启用</el-button>
</#if>
          <el-button v-waves v-permission="['${cfg.cjmModuleName}:${entity?uncap_first}:delete']" type="danger" @click="handleDelete${entity}(scope.$index, scope.row)">删除</el-button>
        </template>
      <#if hasChild>
      </tree-table-list>
      <#else>
      </base-list></#if>
      <el-dialog :title="dialogTextMap[dialogStatus]" :visible.sync="createDialogVisible" :width="$store.state.app.device === 'mobile' ? '100%' : '60%'">
        <el-form ref="dataForm" :rules="rules" :model="temp" label-width="100px" label-suffix=": "<#if table.fields!?size gt 5> :inline="true"</#if>>
<#list table.fields as field>
    <#if field.propertyName!="parentId" && field.propertyName!="level" && !field.keyFlag>
          <el-form-item label="${field.comment}" prop="${field.propertyName}">
      <#if field.propertyName?lower_case == "createtime" ||field.propertyName?lower_case == "createdate" || field.propertyName?lower_case == "updatetime" || field.propertyName?lower_case == "updatedate">
      <#elseif field.propertyName?lower_case?ends_with("image")>
            <upload-image class="form-item"  ref="${field.propertyName}" @uploadSuccess="mediaChange" :fileUrl="temp.${field.propertyName}" columnName="${field.propertyName}"></upload-image>
      <#elseif field.propertyName?lower_case?contains("video")>
            <upload-video class="form-item"  ref="${field.propertyName}" @uploadSuccess="mediaChange" :fileUrl="temp.${field.propertyName}" columnName="${field.propertyName}"></upload-video>
      <#elseif field.propertyName?ends_with("Id") && field.propertyName != "parentId" && field.propertyType == "Integer">
            <el-select clearable class="form-item" v-model="temp.${field.propertyName}" placeholder="${field.comment}" filterable>
              <el-option v-for="item in ${field.propertyName?remove_ending("Id")}Options" :key="item.id" :label="item.name" :value="item.id"></el-option>
            </el-select>
      <#elseif field.propertyName == "status">
            <el-select clearable class="form-item" v-model="temp.${field.propertyName}" placeholder="${field.comment}" filterable>
              <el-option v-for="item in statusOptions" :key="item.id" :label="item.name" :value="item.id"></el-option>
            </el-select>
      <#elseif field.propertyType == "Integer">
            <el-input-number clearable class="form-item" v-model="temp.${field.propertyName}" placeholder="${field.comment}"></el-input-number>
      <#elseif field.propertyType == "LocalDateTime">
            <el-date-picker clearable class="form-item-time" style="width: 365px" v-model="temp.${field.propertyName}" type="datetime" format="yyyy-MM-dd HH:mm:ss" value-format="yyyy-MM-dd HH:mm:ss" placeholder="${field.comment}"></el-date-picker>
      <#elseif field.propertyType == "LocalTime">
            <el-time-select clearable class="form-item-time" style="width: 365px" v-model="temp.${field.propertyName}" :picker-options="{ start: '00:00', step: '00:15', end: '24:00' }" placeholder="${field.comment}"></el-time-select>
      <#elseif field.propertyType == "LocalDate">
            <el-date-picker clearable class="form-item-time" style="width: 365px" v-model="temp.${field.propertyName}" type="date" format="yyyy-MM-dd" value-format="yyyy-MM-dd" placeholder="${field.comment}"></el-date-picker>
      <#elseif field.propertyType == "BigDecimal" || field.propertyName?lower_case?contains("longitude") || field.propertyName?lower_case?contains("latitude") >
            <el-input clearable class="form-item" v-model="temp.${field.propertyName}" placeholder="${field.comment}"  onkeyup="value=value.replace(/[^\d\.]/g,'').replace('.','$#$').replace(/\./g,'').replace('$#$','.')"></el-input>
      <#else>
        <#if field.propertyName?lower_case?ends_with("describe")>
            <el-input clearable class="form-item" v-model="temp.${field.propertyName}" placeholder="${field.comment}" type="textarea" :rows="5" autosize></el-input>
        <#elseif field.propertyName?lower_case?ends_with("content")>
            <editor-quill columnName="${field.propertyName}" ref="${field.propertyName}" :editorContext="temp.${field.propertyName}" @textHtmlChange="contentChange" placeholder="${field.comment}"></editor-quill>
        <#else>
            <el-input clearable class="form-item" v-model="temp.${field.propertyName}" placeholder="${field.comment}"></el-input>
        </#if>
      </#if>
          </el-form-item>
    </#if>
</#list>
        </el-form>
        <div slot="footer" class="dialog-footer">
          <el-button v-waves @click="createDialogVisible = false">取消</el-button>
          <el-button v-waves type="primary" v-permission="['${cfg.cjmModuleName}:${entity?uncap_first}:add' || '${entity?uncap_first}:edit ']" ::loading="saveLoading" @click="dialogStatus==='create'?handleAdd${entity}():handleUpdate${entity}()">提交</el-button>
        </div>
      </el-dialog>
<#if table.fields!?size gt cfg.minExport>
      <el-dialog :title="dialogTextMap[dialogStatus]" :visible.sync="detailDialogVisible" :width="$store.state.app.device === 'mobile' ? '100%' : '60%'">
        <form-detail ref="detailForm" :columns="detailColumns" :dataForm="temp"  :format="format"></form-detail>
      </el-dialog>
</#if>
    </div>
  </div>
</template>

<script>
import permission from '@/directive/permission/index.js' // 权限判断指令
import waves from '@/directive/waves' // waves directive
import { create as create${entity}, update as update${entity}, deleteById as delete${entity},<#if hasStatus> updateSate as set${entity}State,</#if> <#if hasSort>changeSort as change${entity}Sort,</#if><#if !hasChild><#if table.fields!?size gt 10>exportExcel as export${entity}Excel, detail as get${entity}Detail </#if></#if>} from '@/api/${cfg.cjmModuleName}/${entity?uncap_first}'
<#list table.fields as field>
  <#if field.propertyName?ends_with("Id") && field.propertyName != "parentId" && field.propertyType == "Integer">
    <#assign optionsKey=field.propertyName?remove_ending("Id")?uncap_first/>
import { getOptions as get${field.propertyName?remove_ending("Id")?cap_first}Options } from '@/api/${cfg.tableModuleMap[optionsKey]!''}/${optionsKey}'
  </#if>
</#list>
<#if hasChild>
import TreeTableList from '@/components/TreeTableList'
<#else>
import BaseList from '@/components/BaseList'
</#if>
<#if hasVideo>
import UploadVideo from '@/components/UploadVideo'
</#if>
<#if hasImage>
import UploadImage from '@/components/UploadImage'
</#if>
<#if table.fields!?size gt cfg.minExport>
import FormDetail from '@/components/FormDetail'
import {getToken} from "@/utils/auth";
</#if>
<#if hasContent>
import EditorQuill from '@/components/EditorQuill'
</#if>
export default {
  name: '${entity}',
  components: { <#if hasChild>TreeTableList<#else>BaseList</#if><#if hasVideo>, UploadVideo</#if><#if hasImage>, UploadImage</#if><#if table.fields!?size gt 10>, FormDetail</#if><#if hasContent>, EditorQuill</#if> },
  directives: { permission, waves },
  data() {
    return {
      list: null,
      columns: [
<#if !hasChild>
        { key: 'index', label: '编号', width: 80, fixed: 'left' },
</#if>
<#list table.fields as field>
  <#if field.keyFlag>
  <#elseif field.propertyName == "parentId" || field.propertyName=="level" || field.propertyName=="password">
  <#elseif field.propertyName!="status" && field.propertyType == "Integer">
        { key: '${field.propertyName}', label: '${field.comment}' },
  <#else>
        { key: '${field.propertyName}', label: '${field.comment}'<#if field.propertyType != "String">, sortable: true </#if> <#if field.propertyName?lower_case?contains("status")>, type:'status' </#if><#if field.propertyName == "sortBy">, type:'sort' </#if><#if field.propertyName?lower_case?contains("image") || field.propertyName?lower_case?contains("photo") || field.propertyName?lower_case?contains("picture")>, type:'image' </#if><#if field.propertyName?lower_case?contains("video") || field.propertyName?lower_case?contains("mp4") || field.propertyName?lower_case?contains("avi")>, type:'video' </#if>},
  </#if>
</#list>
<#if superEntityClass??>
        { key: 'createTime', label: '创建时间',sortable: true },
</#if>
    <#if table.fields!?size gt cfg.minExport || hasContent>
        { key: 'actions', label: '操作', width: 270, fixed: 'right' }
    <#else>
        { key: 'actions', label: '操作', width: 180, fixed: 'right' }
    </#if>
      ],
<#if table.fields!?size gt cfg.minExport>
      detailColumns: [
        {label:'', columns: [
  <#list table.fields as field>
            { key: '${field.propertyName}', label: '${field.comment}' },
  </#list>
  <#if superEntityClass??>
            { key: 'createTime', label: '创建时间' },
            { key: 'updateTime', label: '修改时间' },
</#if>
        ]}
      ],
      requestData: {
        requestUrl: process.env.VUE_APP_BASE_API + '/${cfg.cjmModuleName}/${entity?uncap_first}/import',
        headerToken: {'Auth-Token': getToken()}
      },
</#if>
      format: {
<#if !hasChild>
        index: (data, index) => { return index + 1 },
</#if>
<#list table.fields as field>
  <#if field.propertyName?ends_with("Id") && field.propertyName != "parentId" && field.propertyType == "Integer">
        ${field.propertyName}: (data, index) => {return (this.${field.propertyName?remove_ending("Id")}Options.find(e => e.id === data.${field.propertyName})||{name:''}).name},
  </#if>
</#list>
      },
<#if hasChild>
      listQuery: {
  <#list table.fields as field>
        ${field.propertyName}: undefined<#if table.fields?last.propertyName != field.propertyName>,</#if>
  </#list>
      },
<#else>
      listQuery: [
    <#list table.fields as field>
        { column: '${field.propertyName}', condition: '<#if field.propertyType == "LocalDateTime">bt<#elseif field.propertyType == "String">like<#else>=</#if>', value: null },
    </#list>
    <#if superEntityClass??>
        { column: 'createTime', condition: 'bt', value: null }
    </#if>
      ],
</#if>
      temp: {<#list table.fields as field>${field.propertyName}: undefined<#if table.fields?last.propertyName != field.propertyName>,</#if></#list>},
      saveLoading: false,
<#list table.fields as field>
  <#if field.propertyName=="status">
      statusOptions: [{ id: 1, name: '启用' }, { id: 2, name: '禁用' }],
  <#elseif field.propertyName?ends_with("Id") && field.propertyName != "parentId" && field.propertyType == "Integer">
      ${field.propertyName?remove_ending("Id")}Options: [],
  </#if>
</#list>
      dialogTextMap: {update: '编辑',create: '新建', detail: '详情'},
      rules: {
<#list table.fields as field>
        ${field.propertyName}: [{ required: true, message: '${field.comment}必填!', trigger: '<#if field.propertyType=='String' || field.propertyType=="BigDecimal">blur<#else>change</#if>' }],
</#list>
      },
      dialogStatus: '',
      createDialogVisible: false,
<#if table.fields!?size gt cfg.minExport>
      detailDialogVisible: false
</#if>
    }
  },
  created() {
    const allOptions = JSON.parse(localStorage.getItem('allOptions'))
<#if hasOptions>
    this.handleOptions()
</#if>
  },
  mounted() {
    this.fetchListData()
  },
  methods: {
    fetchListData() {
      this.$refs.table.fetchData(1)
    },
    reloadListData() {
      this.$refs.table.fetchData()
    },
<#if hasOptions>
    handleOptions() {
  <#list table.fields as field>
    <#if field.propertyName?ends_with("Id") && field.propertyName != "parentId" && field.propertyType == "Integer">
      <#assign optionsKey=field.propertyName?remove_ending("Id")?uncap_first/>
      if (permission.check(['${cfg.tableModuleMap[optionsKey]!''}:${optionsKey}:options'])) {
        get${field.propertyName?remove_ending("Id")?cap_first}Options().then(res => {
          this.${field.propertyName?remove_ending("Id")}Options = res.data
        })
      }
    </#if>
  </#list>
    },
</#if>
    resetTemp() {
      this.temp = {<#list table.fields as field>${field.propertyName}: <#if field.propertyName == "status">1<#else>undefined</#if>,</#list>}
    },
<#if hasImage || hasVideo>
    mediaChange(val, index, column) {
      this.temp[column] = val;
    },
</#if>
<#if hasContent>
    contentChange(val, index, columnName) {
      this.temp[columnName] = val
    },
</#if>
    handleCreate${entity}(<#if hasChild>parentId<#if hasLevel>, level=1</#if></#if>) {
      this.resetTemp()
<#if hasChild>
      this.temp.parentId = parentId
  <#if hasLevel>
      this.temp.level = level
  </#if>
</#if>
      this.dialogStatus = 'create'
      this.createDialogVisible = true
      this.$nextTick(() => {
        this.$refs['dataForm'].clearValidate()
      })
    },
<#if table.fields!?size gt cfg.minExport || hasContent>
    handleDetail${entity}(index, val) {
      get${entity}Detail(val.id).then(res => {
        this.temp = Object.assign({}, res.data)
        this.dialogStatus = 'detail'
        this.detailDialogVisible = true
      })
    },
    handleEdit${entity}(index, val) {
      get${entity}Detail(val.id).then(res => {
        this.temp = Object.assign({}, res.data)
        this.dialogStatus = 'update'
        this.createDialogVisible = true
        this.$nextTick(() => {
          this.$refs['dataForm'].clearValidate()
        })
      })
    },
<#else>
    handleEdit${entity}(index, val) {
      this.temp = Object.assign({}, val)
      this.dialogStatus = 'update'
      this.createDialogVisible = true
      this.$nextTick(() => {
        this.$refs['dataForm'].clearValidate()
      })
    },
</#if>
    handleAdd${entity}() {
      this.$refs['dataForm'].validate((valid) => {
        if (valid) {
          this.saveLoading = true
          const queryData = Object.assign({}, this.temp)
          queryData.status = Number(queryData.status)
          create${entity}(queryData).then(() => {
            // this.list.unshift(queryData)
            this.fetchListData()
            this.createDialogVisible = false
            this.saveLoading = false
            this.$notify.success('${table.comment!}创建成功!')
          }).catch(error => {
            this.saveLoading = false
          })
        }
      })
    },
    handleUpdate${entity}() {
      this.$refs['dataForm'].validate((valid) => {
        if (valid) {
          this.saveLoading = true
          const tempData = Object.assign({}, this.temp)
          tempData.timestamp = +new Date(tempData.timestamp)
          update${entity}(tempData).then(() => {
            this.reloadListData()
            this.createDialogVisible = false
            this.saveLoading = false
            this.$notify.success('${table.comment!}更新成功!')
          }).catch(error => {
            this.saveLoading = false
          })
        }
      })
    },
<#if !hasChild>
    <#if table.fields!?size gt cfg.minExport>
    handleImportSuccess(res, file, fileList) {
      console.error(res)
      this.reloadListData()
      this.$notify.success('文件上传成功')
    },
    handleImportFail(err, file, fileList) {
      console.error(err)
      this.$notify.error('文件上传失败, 请检查网络后重试')
    },
    handleExport${entity}() {
      const conditions = this.listQuery.filter(e => e.value)
      export${entity}Excel({request: encodeURI(JSON.stringify({ conditions: conditions }))})
    },
  </#if>
</#if>
    handleDelete${entity}(index, val) {
      const self = this
      this.$confirm('确定要删除该${table.comment!}？', '警告', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(() => {
        delete${entity}(val.id).then(() => {
          self.reloadListData()
          self.$notify.success('${table.comment!}已删除!')
        })
      }).catch(() => {})
    },
<#if hasSort>
    handleChangeSortBy(oneId, oneSortBy, twoId, twoSortBy) {
      const sortChangeData = { oneId: oneId, oneSortBy: oneSortBy, twoId: twoId, twoSortBy: twoSortBy }
      change${entity}Sort(sortChangeData).then(res => {
        this.fetchListData()
      })
    },
</#if>
<#if hasStatus>
    handleStatus${entity}(id, status) {
      const self = this
      this.$confirm('确定要'+['', '启用','禁用'][status]+'该${table.comment!}？', '警告', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(() => {
        set${entity}State(id, status).then(() => {
          self.createDialogVisible = false
          self.reloadListData()
          self.$notify.success('${table.comment!}'+['', ' 已启用!', '已禁用!'][status])
        })
      }).catch(() => {return})
    },
</#if>
  }
}
</script>

