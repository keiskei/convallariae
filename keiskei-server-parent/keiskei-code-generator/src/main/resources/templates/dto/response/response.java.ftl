<#assign hasChild=false>
<#list table.fields as field>
    <#if field.propertyName == "parentId"><#assign hasChild=true></#if>
</#list>
package cn.jx.cjm.${cfg.cjmModuleName}.dto.response;

import java.math.BigDecimal;
import lombok.Data;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

<#if hasChild>
import cn.jx.cjm.common.base.ParentEntity;
</#if>
import cn.jx.cjm.common.util.DateTimeUtils;
import cn.jx.cjm.common.util.LocalDateTimeDeserializer;
import cn.jx.cjm.common.util.LocalDateTimeSerializer;
import cn.jx.cjm.common.util.LocalDateDeserializer;
import cn.jx.cjm.common.util.LocalDateSerializer;
import cn.jx.cjm.common.util.LocalTimeDeserializer;
import cn.jx.cjm.common.util.LocalTimeSerializer;
import com.fasterxml.jackson.annotation.JsonFormat;
import cn.jx.cjm.common.enums.StatusEnum;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;


/**
 * <p>
 * ${table.comment!} admin 返回类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */

@Data
@ApiModel(value="${entity}Response", description="${table.comment!} 返回类")
public class ${entity}Response <#if hasChild>implements ParentEntity<${entity}Response> </#if> implements Serializable{

    private static final long serialVersionUID = -1L;

<#if superEntityClass??>
    @ApiModelProperty(value = "${table.comment!?trim?replace("\"","'")}id", dataType="${cfg.idType}")
    private ${cfg.idType} id;

</#if>
<#-- ----------  BEGIN 字段循环遍历  ---------->
<#list table.fields as field>
    @ApiModelProperty(value = "${field.comment}", dataType="${field.propertyType}")
    <#if field.propertyType?lower_case?contains("time")>
    @JsonDeserialize(using = ${field.propertyType}Deserializer.class)
    @JsonSerialize(using = ${field.propertyType}Serializer.class)
    </#if>
    private ${field.propertyType} ${field.propertyName};
    <#if field.propertyName?ends_with("Id") && !hasChild && field.propertyType == "Integer">
    @ApiModelProperty(value = "${field.comment?trim?replace("\"","'")}", dataType="String")
    private String ${field.propertyName?replace("Id","Name")};

    </#if>
</#list>
<#------------  END 字段循环遍历  ---------->
<#if hasChild>
    @ApiModelProperty(value = "子集", dataType="List")
    private List<${entity}Response> children;

</#if>
<#if superEntityClass??>
    @ApiModelProperty(value = "创建时间", dataType="String")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间", dataType="String")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime updateTime;

</#if>
<#if table.fields!?size gt cfg.minExport>
    private static List<String> TITLES;
    static {
        TITLES = new ArrayList<>();
        TITLES.add("编号");
    <#list table.fields as field>
        TITLES.add("${field.comment}");
    </#list>
    <#if superEntityClass??>
        TITLES.add("创建时间");
        TITLES.add("修改时间");
    </#if>

    }
    public static List<List<String>> dataList(List<${entity}Response> dataList) {
        int listSize = dataList.size();
        List<List<String>> result = new ArrayList<>(listSize + 1);
        result.add(TITLES);
        int i = 0;
        for (${entity}Response temp : dataList) {
            List<String> tempList = new ArrayList<>();
            tempList.add(String.valueOf(++i));
    <#list table.fields as field>
        <#if field.propertyName == "status">
            tempList.add(null == temp.status ? "" : StatusEnum.get(temp.status));
        <#elseif field.propertyType == "LocalDateTime">
            tempList.add(null == temp.${field.propertyName} ? "" : DateTimeUtils.localDateToStr(temp.${field.propertyName}));
        <#elseif field.propertyName?ends_with("Id")  && !hasChild && field.propertyType == "Integer">
            tempList.add(null == temp.${field.propertyName?replace("Id","Name")} ? "" : temp.${field.propertyName?replace("Id","Name")});
        <#elseif field.propertyType != "String">
            tempList.add(null == temp.${field.propertyName} ? "" : String.valueOf(temp.${field.propertyName}));
        <#else>
            tempList.add(null == temp.${field.propertyName} ? "" : temp.${field.propertyName});
        </#if>
    </#list>
        <#if superEntityClass??>
            tempList.add(null == temp.createTime ? "" : DateTimeUtils.localDateToStr(temp.createTime));
            tempList.add(null == temp.updateTime ? "" : DateTimeUtils.localDateToStr(temp.updateTime));
        </#if>
            result.add(tempList);
        }
        return result;
    }
</#if>
}
