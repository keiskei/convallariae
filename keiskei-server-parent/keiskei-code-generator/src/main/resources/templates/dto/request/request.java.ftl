package cn.jx.cjm.${cfg.cjmModuleName}.dto.request;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.LocalDate;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import cn.jx.cjm.common.util.LocalDateTimeDeserializer;
import cn.jx.cjm.common.util.LocalDateTimeSerializer;
import cn.jx.cjm.common.util.LocalDateDeserializer;
import cn.jx.cjm.common.util.LocalDateSerializer;
import cn.jx.cjm.common.util.LocalTimeDeserializer;
import cn.jx.cjm.common.util.LocalTimeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

<#if entityLombokModel>
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
</#if>
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.SelectKey;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * ${table.comment!} admin 接收类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */


@Data
@ApiModel(value="${entity}Request", description="${table.comment!} 接受类")
public class ${entity}Request implements Serializable {

    private static final long serialVersionUID = -1L;

<#-- ----------  BEGIN 字段循环遍历  ---------->
<#if superEntityClass??>
    @ApiModelProperty(value = "${table.comment!?trim?replace("\"","'")}id", dataType="${cfg.idType}")
    @NotNull(message = "请选择${table.comment!?trim?replace("\"","'")}", groups = {Update.class, SelectKey.class})
    private ${cfg.idType} id;

</#if>
<#list table.fields as field>
    <#if field.keyFlag>
        <#assign keyPropertyName="${field.propertyName}"/>
    </#if>
    @ApiModelProperty(value = "${field.comment?trim?replace("\"","'")}", dataType="${field.propertyType}")
    <#if field.propertyName!="parentId" && field.propertyName!="level">
        <#if field.propertyType == "String">
    @NotBlank(message = "请输入${field.comment?trim?replace("\"","'")}", groups = {<#if field.keyFlag>SelectKey.class, Update.class<#else>Insert.class</#if>})
        <#else>
    @NotNull(message = "请选择${field.comment?trim?replace("\"","'")}", groups = {<#if field.keyFlag>SelectKey.class, Update.class<#else>Insert.class</#if>})
        </#if>
    </#if>
    <#if field.propertyType?lower_case?contains("time")>
    @JsonDeserialize(using = ${field.propertyType}Deserializer.class)
    </#if>
    private ${field.propertyType} ${field.propertyName};

</#list>
<#------------  END 字段循环遍历  ---------->

<#if entityColumnConstant>
    <#list table.fields as field>
        public static final String ${field.name?upper_case} = "${field.name}";

    </#list>
</#if>
<#if activeRecord>
    @Override
    protected Serializable pkVal() {
    <#if keyPropertyName??>
        return this.${keyPropertyName};
    <#else>
        return null;
    </#if>
    }

</#if>
}
