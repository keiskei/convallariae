package cn.jx.cjm.security.mapper;

import cn.jx.cjm.security.entity.SysUserDepartment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 管理员信息部门 Mapper 接口
 * </p>
 *
 * @author James Chen right_way@foxmail.com
 * @since 2020-10-16
 */
public interface SysUserDepartmentMapper extends BaseMapper<SysUserDepartment> {

}
