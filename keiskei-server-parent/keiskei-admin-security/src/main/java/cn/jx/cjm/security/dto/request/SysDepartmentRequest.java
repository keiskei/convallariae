package cn.jx.cjm.security.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 部门 admin 接收类
 * </p>
 *
 * @author James Chen right_way@foxmail.com
 * @since 2020-10-16
 */


@Data
@ApiModel(value = "SysDepartmentRequest", description = "部门 后台接受类")
public class SysDepartmentRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "部门id", dataType = "Integer")
    @NotNull(message = "请选择部门", groups = {Update.class, SelectKey.class})
    private Integer id;

    @ApiModelProperty(value = "上级部门", dataType = "Integer")
    private Integer parentId;

    @ApiModelProperty(value = "部门名称", dataType = "String")
    @NotBlank(message = "请输入部门名称", groups = {Insert.class})
    private String name;

}
