package cn.jx.cjm.security.dto.request;

import cn.jx.cjm.common.util.LocalDateTimeDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 管理员 admin 接收类
 * </p>
 *
 * @author James Chen right_way@foxmail.com
 * @since 2020-10-16
 */


@Data
@ApiModel(value = "SysUserSelfRequest", description = "管理员 后台接受类")
public class SysUserSelfRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "头像", dataType = "String")
    @NotBlank(message = "请输入头像", groups = {Update.class})
    private String avatar;

    @ApiModelProperty(value = "真实姓名", dataType = "String")
    @NotBlank(message = "请输入真实姓名", groups = {Update.class})
    private String realName;

    @ApiModelProperty(value = "手机号", dataType = "String")
    @NotBlank(message = "请输入手机号", groups = {Update.class})
    private String phone;

    @ApiModelProperty(value = "邮箱", dataType = "String")
    @NotBlank(message = "请输入邮箱", groups = {Update.class})
    @Email(message = "邮箱格式错误", groups = {Update.class})
    private String email;

    @ApiModelProperty(value = "登录用户名", dataType = "String")
    @NotBlank(message = "请输入登录用户名", groups = {UpdateProvider.class})
    private String username;

    @ApiModelProperty(value = "密码", dataType = "String")
    @NotBlank(message = "请输入密码", groups = {UpdateProvider.class})
    private String password;

    @ApiModelProperty(value = "新密码", dataType = "String")
    @NotBlank(message = "请输入新密码", groups = {UpdateProvider.class})
    private String newPassword;





}
