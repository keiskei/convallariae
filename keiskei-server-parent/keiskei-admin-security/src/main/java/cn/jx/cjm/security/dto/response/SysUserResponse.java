package cn.jx.cjm.security.dto.response;

import cn.jx.cjm.common.util.LocalDateTimeDeserializer;
import cn.jx.cjm.common.util.LocalDateTimeSerializer;
import cn.jx.cjm.common.vo.router.Router;
import cn.jx.cjm.security.common.bean.CjmGrantedAuthority;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;


/**
 * <p>
 * 管理员 admin 返回类
 * </p>
 *
 * @author James Chen right_way@foxmail.com
 * @since 2020-10-16
 */

@Data
@ApiModel(value = "SysUserResponse", description = "管理员 后台返回类")
public class SysUserResponse {

    @ApiModelProperty(value = "管理员id", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "登录用户名", dataType = "String")
    private String username;


    @ApiModelProperty(value = "密码", dataType = "String")
    @JsonIgnore
    private String password;


    @ApiModelProperty(value = "真实姓名", dataType = "String")
    private String realName;


    @ApiModelProperty(value = "最后登录时间", dataType = "LocalDateTime")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime lastLoginTime;


    @ApiModelProperty(value = "最后修改密码时间", dataType = "LocalDateTime")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime lastModifyPasswordTime;


    @ApiModelProperty(value = "密码错误次数", dataType = "Integer")
    private Integer passwordErrorTimes;


    @ApiModelProperty(value = "锁定时间", dataType = "LocalDateTime")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime accountLockTime;


    @ApiModelProperty(value = "账号过期时间", dataType = "LocalDateTime")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime accountExpiredTime;


    @ApiModelProperty(value = "状态", dataType = "Integer")
    private Integer status;


    @ApiModelProperty(value = "头像", dataType = "String")
    private String avatar;


    @ApiModelProperty(value = "手机号", dataType = "String")
    private String phone;


    @ApiModelProperty(value = "邮箱", dataType = "String")
    private String email;


    @ApiModelProperty(value = "创建时间", dataType = "String")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间", dataType = "String")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "角色ID", dataType="List<Integer>")
    private List<Integer> sysRoleIds;

    @ApiModelProperty(value = "部门ID", dataType="List<Integer>")
    private List<Integer> sysDepartmentIds;

}
