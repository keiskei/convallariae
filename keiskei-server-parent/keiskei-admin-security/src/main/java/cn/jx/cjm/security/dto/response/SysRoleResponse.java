package cn.jx.cjm.security.dto.response;

import cn.jx.cjm.common.util.LocalDateTimeDeserializer;
import cn.jx.cjm.common.util.LocalDateTimeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;


/**
 * <p>
 * 角色信息 admin 返回类
 * </p>
 *
 * @author James Chen
 * @since 2019-12-16
 */

@Data
@ApiModel(value = "SysDepartmentResponse", description = "角色信息 后台返回类")
public class SysRoleResponse implements Serializable {

    private static final long serialVersionUID = 6310149436276096689L;


    @ApiModelProperty(value = "角色信息id", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "角色名称", dataType = "String")
    private String authority;

    @ApiModelProperty(value = "状态", dataType = "Integer")
    private Integer status;

    private Collection<Integer> permissionIds;

    @ApiModelProperty(value = "创建时间", dataType = "String")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间", dataType = "String")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime updateTime;

}
