package cn.jx.cjm.security.entity;

import cn.jx.cjm.common.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 后台权限详情信息
 * </p>
 *
 * @author 陈加敏 right_way@foxmail.com
 * @since 2019-06-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_permission")
public class SysPermission extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 权限标识
     */
    private String authority;

    /**
     * 资源名称
     */
    private String authorityName;

    /**
     * 请求url路径
     */
    private String authorityPath;

    /**
     * 请求方式 post/get...
     */
    private String authorityMethod;

    /**
     * 路由名称
     */
    private String routerName;

    /**
     * 路由路径
     */
    private String routerPath;

    /**
     * 路由跳转
     */
    private String routerRedirect;

    /**
     * 路由页面
     */
    private String routerComponent;

    /**
     * 路由标题
     */
    private String routerTitle;

    /**
     * 路由图标
     */
    private String routerIcon;

    /**
     * 路由是否缓存
     */
    private Boolean routerNoCache;

    /**
     * 排序
     */
    private Integer sortBy;

    /**
     * 父级菜单
     */
    private Integer parentId;

    /**
     * 路由类型
     */

    private String type;
    /**
     * 是否隐藏
     */
    private Boolean hidden;


}
