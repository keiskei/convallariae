package cn.jx.cjm.security.common.util;

import cn.jx.cjm.common.util.BeanUtils;
import cn.jx.cjm.common.vo.router.Router;
import cn.jx.cjm.common.vo.router.RouterMeta;
import cn.jx.cjm.security.dto.response.SysPermissionResponse;
import cn.jx.cjm.security.entity.SysPermission;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 路由/权限树工具类
 *
 * @author 陈加敏
 * @since 2019/7/22 16:17
 */
public class RouterUtils {

    private final Map<Integer, List<SysPermission>> sysPermissionMap;
    private final Set<String> permissions;
    private Set<Integer> permissionIdTree;

    public RouterUtils(List<SysPermission> sysPermissions) {
        permissions = new HashSet<>(sysPermissions.size());

        //通过父级id对权限分组
        sysPermissionMap = new LinkedHashMap<>();
        for (SysPermission sysPermission : sysPermissions) {
            //拼装前端页面权限
            permissions.add(sysPermission.getAuthority());

            Integer parentId = sysPermission.getParentId();
            if (sysPermissionMap.containsKey(parentId)) {
                sysPermissionMap.get(parentId).add(sysPermission);
            } else {
                List<SysPermission> sysPermissionListTemp = new ArrayList<>();
                sysPermissionListTemp.add(sysPermission);
                sysPermissionMap.put(parentId, sysPermissionListTemp);
            }
        }
    }

    public Set<Integer> getPermissionIdTree(Integer parenId) {
        permissionIdTree = new HashSet<>();
        Set<Integer> parentIds = new HashSet<>(1);
        parentIds.add(parenId);
        return getPermissionIdTree(parentIds);
    }

    private Set<Integer> getPermissionIdTree(Set<Integer> parentIds) {
        permissionIdTree.addAll(parentIds);
        Set<Integer> idsTemp = new HashSet<>();
        for (Integer parentId : parentIds) {
            List<SysPermission> sysPermissions = sysPermissionMap.get(parentId);
            if (! CollectionUtils.isEmpty(sysPermissions)) {
                Set<Integer> childIdsTemp = sysPermissions.stream().map(SysPermission::getId).collect(Collectors.toSet());
                idsTemp.addAll(childIdsTemp);
            }
        }
        if (!CollectionUtils.isEmpty(idsTemp)) {
            return getPermissionIdTree(idsTemp);
        }else {
            return permissionIdTree;
        }
    }

    /**
     * 创建路由树
     *
     * @return .
     */
    public List<Router> createRouter() {

        return getRouter(null);
    }

    /**
     * 返回页面权限
     *
     * @return .
     */
    public Set<String> createPermissions() {
        return permissions;
    }

    /**
     * 创建权限树
     *
     * @return .
     */
    public List<SysPermissionResponse> createSysPermissionList() {
        return getSysPermissionList(null);
    }

    /**
     * 拼装权限树
     *
     * @param key 父级id
     * @return .
     */
    private List<SysPermissionResponse> getSysPermissionList(Integer key) {
        List<SysPermission> sysPermissions = sysPermissionMap.get(key);
        if (CollectionUtils.isEmpty(sysPermissions)) {
            return null;
        }
        List<SysPermissionResponse> sysPermissionResponses = new ArrayList<>(sysPermissions.size());
        for (SysPermission sysPermission : sysPermissions) {
            SysPermissionResponse response = new SysPermissionResponse();
            BeanUtils.copyPropertiesIgnoreNull(sysPermission, response);
            //方法回调
            response.setChildren(getSysPermissionList(response.getId()));
            sysPermissionResponses.add(response);
        }
        return sysPermissionResponses;

    }

    /**
     * 拼装路由
     *
     * @param key 父级id
     * @return .
     */
    private List<Router> getRouter(Integer key) {

        List<SysPermission> sysPermissions = sysPermissionMap.get(key);
        if (CollectionUtils.isEmpty(sysPermissions)) {
            return null;
        }
        List<Router> routers = new ArrayList<>(sysPermissions.size());

        for (SysPermission sysPermission : sysPermissions) {
            //不记录按钮值
            if ("button".equals(sysPermission.getType())) {
                continue;
            }

            RouterMeta routerMeta = new RouterMeta();
            routerMeta.setIcon(sysPermission.getRouterIcon());
            routerMeta.setNoCache(sysPermission.getRouterNoCache());
            routerMeta.setTitle(sysPermission.getRouterTitle());


            Router router = new Router();
            router.setPath(sysPermission.getRouterPath());
            router.setComponent(sysPermission.getRouterComponent());
            router.setHidden(sysPermission.getHidden());
            router.setName(sysPermission.getRouterName());
            router.setRedirect(sysPermission.getRouterRedirect());
            router.setMeta(routerMeta);
            //方法回调
            router.setChildren(getRouter(sysPermission.getId()));
            routers.add(router);
        }
        if (CollectionUtils.isEmpty(routers)) {
            return null;
        }
        return routers;
    }
}
