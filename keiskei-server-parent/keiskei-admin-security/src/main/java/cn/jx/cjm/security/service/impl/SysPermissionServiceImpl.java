package cn.jx.cjm.security.service.impl;

import cn.jx.cjm.security.entity.SysPermission;
import cn.jx.cjm.security.mapper.SysPermissionMapper;
import cn.jx.cjm.security.service.ISysPermissionService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * <p>
 * 后台权限详情信息 服务实现类
 * </p>
 *
 * @author 陈加敏 right_way@foxmail.com
 * @since 2019-06-24
 */
@Service
public class SysPermissionServiceImpl extends ServiceImpl<SysPermissionMapper, SysPermission> implements ISysPermissionService {
}
