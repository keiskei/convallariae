package cn.jx.cjm.security.dto.response;

import cn.jx.cjm.common.util.LocalDateTimeDeserializer;
import cn.jx.cjm.common.util.LocalDateTimeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;


/**
 * <p>
 * 后台权限详情信息 admin 返回类
 * </p>
 *
 * @author James Chen
 * @since 2019-12-16
 */

@Data
@ApiModel(value = "SysPermissionResponse", description = "后台权限详情信息 后台返回类")
public class SysPermissionResponse implements Serializable {

    private static final long serialVersionUID = 7600639603118493217L;


    @ApiModelProperty(value = "后台权限详情信息id", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "权限标识", dataType = "String")
    private String authority;

    @ApiModelProperty(value = "资源名称", dataType = "String")
    private String authorityName;

    @ApiModelProperty(value = "请求url路径", dataType = "String")
    private String authorityPath;

    @ApiModelProperty(value = "请求方式", dataType = "String")
    private String authorityMethod;

    @ApiModelProperty(value = "路由路径", dataType = "String")
    private String routerPath;

    @ApiModelProperty(value = "路由名称", dataType = "String")
    private String routerName;

    @ApiModelProperty(value = "路由跳转", dataType = "String")
    private String routerRedirect;

    @ApiModelProperty(value = "路由页面", dataType = "String")
    private String routerComponent;

    @ApiModelProperty(value = "路由标题", dataType = "String")
    private String routerTitle;

    @ApiModelProperty(value = "路由图标", dataType = "String")
    private String routerIcon;

    @ApiModelProperty(value = "路由是否缓存", dataType = "Boolean")
    private Boolean routerNoCache;

    @ApiModelProperty(value = "排序", dataType = "Integer")
    private Integer sortBy;

    @ApiModelProperty(value = "父级菜单", dataType = "Integer")
    private Integer parentId;

    @ApiModelProperty(value = "路由类型", dataType = "String")
    private String type;

    @ApiModelProperty(value = "是否隐藏", dataType = "Boolean")
    private Boolean hidden;

    @ApiModelProperty(value = "子集", dataType = "List")
    private List<SysPermissionResponse> children;

    @ApiModelProperty(value = "创建时间", dataType = "String")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间", dataType = "String")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime updateTime;

}
