package cn.jx.cjm.security.controller;

import cn.jx.cjm.security.dto.request.SysRoleRequest;
import cn.jx.cjm.security.dto.response.SysRoleResponse;
import cn.jx.cjm.security.entity.SysRole;
import cn.jx.cjm.security.support.SysRoleSupportService;
import cn.jx.cjm.common.aop.annotation.RequestModel;
import cn.jx.cjm.common.base.BaseRequest;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.api.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 角色信息controller
 *
 * @author 陈加敏
 * @since 2019年7月10日 下午6:31:29
 */
@Api(tags = "后台 - 角色")
@RestController
@RequestMapping("/admin/security/sysRole")
public class SysRoleController {

    @Autowired
    private SysRoleSupportService sysRoleSupportService;

    @ApiOperation("角色信息列表")
    @GetMapping
    public R<IPage<SysRoleResponse>> list(@RequestModel BaseRequest<SysRole> request) {

        return R.ok(sysRoleSupportService.list(request));
    }

    @ApiOperation("角色信息下拉框")
    @GetMapping("/options")
    public R<List<SysRoleResponse>> options() {
        return R.ok(sysRoleSupportService.options());
    }

    @ApiOperation("角色信息详情")
    @GetMapping("/{id}")
    public R<SysRoleResponse> getOne(
            @NotNull @PathVariable("id") Integer id) {
        return R.ok(sysRoleSupportService.getOne(id));
    }

    @ApiOperation("角色信息保存")
    @PostMapping
    public R<Boolean> save(@RequestBody @Validated({Insert.class}) SysRoleRequest request) {
        sysRoleSupportService.save(request);
        return R.ok(true);
    }

    @ApiOperation("角色信息更新")
    @PutMapping
    public R<Boolean> update(@RequestBody @Validated({Update.class}) SysRoleRequest request) {
        sysRoleSupportService.update(request);
        return R.ok(true);

    }

    @ApiOperation("角色信息更新状态")
    @PatchMapping("/{status:[0-3]}")
    public R<Boolean> status(
            @RequestBody @Validated({SelectKey.class}) SysRoleRequest request,
            @PathVariable("status") @NotNull Integer status) {
        request.setStatus(status);
        sysRoleSupportService.update(request);
        return R.ok(true);
    }

    @ApiOperation("角色信息删除")
    @DeleteMapping("/{id}")
    public R<Boolean> remove(@NotNull @PathVariable("id") Integer id) {
        sysRoleSupportService.remove(id);
        return R.ok(true);
    }
}
