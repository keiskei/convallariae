package cn.jx.cjm.security.service;

import cn.jx.cjm.security.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色信息 服务类
 * </p>
 *
 * @author 陈加敏 right_way@foxmail.com
 * @since 2019-06-24
 */
public interface ISysRoleService extends IService<SysRole> {
}
