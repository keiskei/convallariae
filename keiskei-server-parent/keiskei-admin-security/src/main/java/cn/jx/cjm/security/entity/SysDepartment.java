package cn.jx.cjm.security.entity;

import cn.jx.cjm.common.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 部门
 * </p>
 *
 * @author James Chen right_way@foxmail.com
 * @since 2020-10-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_department")
public class SysDepartment extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 上级部门
     */
    private Integer parentId;

    /**
     * 部门名称
     */
    private String name;

    /**
     * 部门标识
     */
    private String rule;

}
