package cn.jx.cjm.security.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 管理员信息部门 admin 接收类
 * </p>
 *
 * @author James Chen right_way@foxmail.com
 * @since 2020-10-16
 */


@Data
@ApiModel(value = "SysUserDepartmentRequest", description = "管理员信息部门 后台接受类")
public class SysUserDepartmentRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "管理员信息部门id", dataType = "Integer")
    @NotNull(message = "请选择管理员信息部门", groups = {Update.class, SelectKey.class})
    private Integer id;

    @ApiModelProperty(value = "管理员", dataType = "Integer")
    @NotNull(message = "请选择管理员", groups = {Insert.class})
    private Integer sysUserId;

    @ApiModelProperty(value = "所属部门", dataType = "Integer")
    @NotNull(message = "请选择所属部门", groups = {Insert.class})
    private Integer sysDepartmentId;


}
