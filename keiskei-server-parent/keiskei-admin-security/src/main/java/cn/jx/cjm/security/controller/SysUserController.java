package cn.jx.cjm.security.controller;

import cn.jx.cjm.common.aop.annotation.RequestModel;
import cn.jx.cjm.common.base.BaseRequest;
import cn.jx.cjm.security.dto.request.SysUserRequest;
import cn.jx.cjm.security.dto.response.SysUserResponse;
import cn.jx.cjm.security.entity.SysUser;
import cn.jx.cjm.security.support.SysUserSupportService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.api.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 管理员信息controller
 *
 * @author 陈加敏
 * @since 2019年7月10日 下午5:35:44
 */
@Api(tags = "后台 - 管理员")
@RestController
@RequestMapping("/admin/security/sysUser")
public class SysUserController {

    @Autowired
    private SysUserSupportService sysUserSupportService;

    @ApiOperation("管理员信息管理员信息列表")
    @GetMapping
    public R<IPage<SysUserResponse>> list(
            @RequestModel BaseRequest<SysUser> request) {
        return R.ok(sysUserSupportService.list(request));
    }

    @ApiOperation("管理员信息下拉框")
    @GetMapping("/options")
    public R<List<SysUserResponse>> options() {

        return R.ok(sysUserSupportService.options());
    }

    @ApiOperation("管理员信息详情")
    @GetMapping("/{id}")
    public R<SysUserResponse> getOne(@NotNull @PathVariable Integer id) {
        return R.ok(sysUserSupportService.getOne(id));
    }

    @ApiOperation("管理员信息保存")
    @PostMapping
    public R<Boolean> save(@RequestBody @Validated({Insert.class}) SysUserRequest request) {
        sysUserSupportService.save(request);
        return R.ok(true);
    }

    @ApiOperation("管理员信息更新")
    @PutMapping
    public R<Boolean> update(@RequestBody @Validated({Update.class}) SysUserRequest request) {
        sysUserSupportService.update(request);
        return R.ok(true);

    }

    @ApiOperation("管理员密码重置")
    @PatchMapping("/resetPassword")
    public R<String> resetPassword(@RequestBody @Validated({SelectKey.class}) SysUserRequest request) {
        return R.ok(sysUserSupportService.resetSysUserPassword(request.getId()));
    }


    @ApiOperation("管理员信息更新状态")
    @PatchMapping("/{status:[1-2]}")
    public R<Boolean> status(@RequestBody @Validated({SelectKey.class}) SysUserRequest request,
                             @PathVariable("status") @NotNull Integer status) {
        SysUserRequest request1 = new SysUserRequest();
        request1.setId(request.getId());
        request1.setStatus(status);
        sysUserSupportService.update(request1);
        return R.ok(true);
    }

    @ApiOperation("管理员信息删除")
    @DeleteMapping("/{id}")
    public R<Boolean> remove(@NotNull @PathVariable Integer id) {
        sysUserSupportService.remove(id);
        return R.ok(true);
    }
}
