package cn.jx.cjm.security.mapper;

import cn.jx.cjm.security.entity.SysPermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 后台权限详情信息 Mapper 接口
 * </p>
 *
 * @author 陈加敏 right_way@foxmail.com
 * @since 2019-06-24
 */
public interface SysPermissionMapper extends BaseMapper<SysPermission> {

}
