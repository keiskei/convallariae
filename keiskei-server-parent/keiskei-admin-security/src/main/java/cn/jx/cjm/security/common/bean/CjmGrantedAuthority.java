package cn.jx.cjm.security.common.bean;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
/**
 * 角色信息
 *
 * @author 陈加敏
 * @since 2019/7/15 13:30
 */
@Data
public class CjmGrantedAuthority implements GrantedAuthority {

	private static final long serialVersionUID = 3173195985587662642L;

	/**
     * 角色ID
     */
    private Integer sysRoleId;

    /**
     * 角色,名称
     */
    private String authority;
}
