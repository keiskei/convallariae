package cn.jx.cjm.security.service.impl;

import cn.jx.cjm.security.entity.SysUserDepartment;
import cn.jx.cjm.security.mapper.SysUserDepartmentMapper;
import cn.jx.cjm.security.service.ISysUserDepartmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 管理员信息部门 服务实现类
 * </p>
 *
 * @author James Chen right_way@foxmail.com
 * @since 2020-10-16
 */
@Service
public class SysUserDepartmentServiceImpl extends ServiceImpl<SysUserDepartmentMapper, SysUserDepartment> implements ISysUserDepartmentService {

}
