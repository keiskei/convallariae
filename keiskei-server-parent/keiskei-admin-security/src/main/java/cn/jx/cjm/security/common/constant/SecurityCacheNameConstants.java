package cn.jx.cjm.security.common.constant;

/**
 * 缓存常量
 *
 * @author 陈加敏
 * @since 2019/7/15 13:37
 */
public class SecurityCacheNameConstants {

    public final static String ROLE_PATH_LIST = "ROLE_PATH_LIST";

    public final static String ROLE_LIST = "ROLE_LIST";

    public final static String DEPARTMENT_LIST = "DEPARTMENT_LIST";
    public final static String SYSTEM_PARAM_LIST = "SYSTEM_PARAM_LIST";
}
