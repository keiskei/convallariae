package cn.jx.cjm.security.common.filter;

import cn.jx.cjm.security.common.util.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author James Chen right_way@foxmail.com
 * @since 2020/9/25 12:37 下午
 */


@Configuration
public class WebMvcConfigurerImpl implements WebMvcConfigurer {

    @Autowired
    private ResponseUtils responseUtils;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new RestApiInterceptor(responseUtils))
                .addPathPatterns("/**")
                .excludePathPatterns("/**/*.css", "/**/*.js", "/**/*.html");
    }
}