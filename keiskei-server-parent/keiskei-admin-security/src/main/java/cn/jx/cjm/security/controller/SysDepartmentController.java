package cn.jx.cjm.security.controller;

import cn.jx.cjm.security.dto.request.SysDepartmentRequest;
import cn.jx.cjm.security.dto.response.SysDepartmentResponse;
import cn.jx.cjm.security.support.SysDepartmentSupportService;
import com.baomidou.mybatisplus.extension.api.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 部门 前端控制器
 *
 * @author James Chen right_way@foxmail.com
 * @since 2020-10-16
 */

@Api(tags = "后台 - 部门")
@RestController
@RequestMapping("/admin/security/sysDepartment")
public class SysDepartmentController {

    @Autowired
    private SysDepartmentSupportService sysDepartmentSupportService;

    @ApiOperation("部门列表")
    @GetMapping
    public R<List<SysDepartmentResponse>> list(
        SysDepartmentRequest request
    ) {
        return R.ok(sysDepartmentSupportService.list(request));
    }

    @ApiOperation("部门下拉框")
    @GetMapping("/options")
    public R<List<SysDepartmentResponse>> options(@RequestParam(required = false) Integer parentId){
        return R.ok(sysDepartmentSupportService.options(parentId));
    }

    @ApiOperation("部门详情")
    @GetMapping("/{id}")
    public R<SysDepartmentResponse> getOne(
        @NotNull(message = "请选择部门") @PathVariable("id") Integer id) {
        return R.ok(sysDepartmentSupportService.getOne(id));
    }

    @ApiOperation("部门保存")
    @PostMapping
    public R<Boolean> save(@RequestBody @Validated({Insert.class}) SysDepartmentRequest request) {
        sysDepartmentSupportService.save(request);
        return R.ok(true);
    }

    @ApiOperation("部门更新")
    @PutMapping
    public R<Boolean> update(@RequestBody @Validated({Update.class}) SysDepartmentRequest request) {
        sysDepartmentSupportService.update(request);
        return R.ok(true);
    }

    @ApiOperation("部门删除")
    @DeleteMapping("/{id}")
    public R<Boolean> remove(
        @NotNull(message = "请选择部门") @PathVariable("id") Integer id) {
        sysDepartmentSupportService.remove(id);
        return R.ok(true);
    }
}

