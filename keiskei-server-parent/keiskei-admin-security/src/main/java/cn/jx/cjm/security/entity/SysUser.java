package cn.jx.cjm.security.entity;

import cn.jx.cjm.common.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * <p>
 * 管理员
 * </p>
 *
 * @author James Chen right_way@foxmail.com
 * @since 2020-10-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_user")
public class SysUser extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 登录用户名
     */
    private String username;

    /**
     * 密码
     */
    @JsonIgnore
    private String password;

    /**
     * 真实姓名
     */
    private String realName;

    /**
     * 最后登录时间
     */
    private LocalDateTime lastLoginTime;

    /**
     * 最后修改密码时间
     */
    private LocalDateTime lastModifyPasswordTime;

    /**
     * 密码错误次数
     */
    private Integer passwordErrorTimes;

    /**
     * 锁定时间
     */
    private LocalDateTime accountLockTime;

    /**
     * 账号过期时间
     */
    private LocalDateTime accountExpiredTime;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;


}
