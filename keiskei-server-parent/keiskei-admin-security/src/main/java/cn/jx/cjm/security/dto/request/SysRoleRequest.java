package cn.jx.cjm.security.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Collection;

/**
 * <p>
 * 角色信息 admin 接收类
 * </p>
 *
 * @author James Chen
 * @since 2019-12-16
 */


@Data
@ApiModel(value = "SysRoleRequest", description = "角色信息 后台请求类")
public class SysRoleRequest implements Serializable {

    private static final long serialVersionUID = 3511492355010459748L;

    @ApiModelProperty(value = "角色信息id", dataType = "Integer")
    @NotNull(message = "请选择角色信息", groups = {Update.class})
    private Integer id;

    @ApiModelProperty(value = "角色名称", dataType = "String")
    @NotBlank(message = "请输入角色名称", groups = {Insert.class, Update.class})
    private String authority;

    @ApiModelProperty(value = "状态", dataType = "Integer")
    @NotNull(message = "请选择状态", groups = {Insert.class, Update.class})
    private Integer status;


    private Collection<Integer> permissionIds;

}
