package cn.jx.cjm.security.controller;

import cn.jx.cjm.security.dto.request.SysPermissionRequest;
import cn.jx.cjm.security.dto.response.SysPermissionResponse;
import cn.jx.cjm.security.support.SysPermissionSupportService;
import cn.jx.cjm.common.base.BaseSortChange;
import com.baomidou.mybatisplus.extension.api.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 权限controller
 *
 * @author 陈加敏
 * @since 2019年7月10日 下午5:14:31
 */
@Api(tags = "后台 - 权限")
@RestController
@RequestMapping("/admin/security/sysPermission")
public class SysPermissionController {


    @Autowired
    private SysPermissionSupportService sysPermissionSupportService;

    @ApiOperation("后台权限详情信息后台权限详情信息列表")
    @GetMapping
    public R<List<SysPermissionResponse>> list() {
        return R.ok(this.sysPermissionSupportService.list());
    }

    @ApiOperation("后台权限详情信息详情")
    @GetMapping("/{id}")
    public R<SysPermissionResponse> getOne(@NotNull @PathVariable("id") Integer id) {
        return R.ok(sysPermissionSupportService.getOne(id));
    }

    @ApiOperation("后台权限详情信息保存")
    @PostMapping
    public R<Boolean> save(@RequestBody @Validated({Insert.class}) SysPermissionRequest request) {
        sysPermissionSupportService.save(request);
        return R.ok(true);
    }

    @ApiOperation("后台权限详情信息更新")
    @PutMapping
    public R<Boolean> update(@RequestBody @Validated({Update.class}) SysPermissionRequest request) {
        sysPermissionSupportService.update(request);
        return R.ok(true);

    }
    @ApiOperation("后台权限详情信息排序")
    @PatchMapping("/sort")
    public R<Boolean> update(@RequestBody @Validated BaseSortChange request) {
        sysPermissionSupportService.sort(request);
        return R.ok(true);

    }

    @ApiOperation("后台权限详情信息删除")
    @DeleteMapping("/{id}")
    public R<Boolean> remove(@NotNull @PathVariable("id") Integer id) {
        sysPermissionSupportService.remove(id);
        return R.ok(true);
    }
}
