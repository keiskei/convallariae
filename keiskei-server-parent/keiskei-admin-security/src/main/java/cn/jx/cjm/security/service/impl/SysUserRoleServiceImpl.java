package cn.jx.cjm.security.service.impl;

import cn.jx.cjm.security.entity.SysUserRole;
import cn.jx.cjm.security.service.ISysUserRoleService;
import cn.jx.cjm.security.mapper.SysUserRoleMapper;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * <p>
 * 用户-角色对应信息 服务实现类
 * </p>
 *
 * @author 陈加敏 right_way@foxmail.com
 * @since 2019-06-24
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {

}
