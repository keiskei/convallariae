package cn.jx.cjm.security.common.handler;

import cn.jx.cjm.security.common.util.ResponseUtils;
import cn.jx.cjm.common.enums.BizExceptionEnum;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.api.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthenticationEntryPointImpl implements AuthenticationEntryPoint {


    @Autowired
    private ResponseUtils responseUtils;

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException {
        responseUtils.send(request, response, JSON.toJSONString(R.failed(BizExceptionEnum.AUTH_FORBIDDEN)));
    }

}
