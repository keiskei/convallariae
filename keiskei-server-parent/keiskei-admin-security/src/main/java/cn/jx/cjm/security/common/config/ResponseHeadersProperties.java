package cn.jx.cjm.security.common.config;

import lombok.Data;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 跨域返回请求头信息
 *
 * @author 陈加敏
 * @since 2019/7/15 13:42
 */
@Component
@ConfigurationProperties(prefix = "keiskei-server-conf.response-headers")
@Data
public class ResponseHeadersProperties {
    private Boolean allowCross = false;
    private String allowOrigin = "*";
    private String allowHeaders = "*";
    private String allowCredentials = "true";
    private String allowMethods = "*";
    private Integer maxAge = 60 * 60 * 24 * 7;
    private String exposeHeaders = "*";
    private String requestHeaders = "*";
    private Integer expires = 60 * 60 * 2;
    private String origin = "*";
    private String cacheControl = "no-cache";
    private String pragma = "no-cache";
}
