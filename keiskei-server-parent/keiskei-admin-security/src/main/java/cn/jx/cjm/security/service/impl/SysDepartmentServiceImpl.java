package cn.jx.cjm.security.service.impl;

import cn.jx.cjm.security.entity.SysDepartment;
import cn.jx.cjm.security.mapper.SysDepartmentMapper;
import cn.jx.cjm.security.service.ISysDepartmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 部门 服务实现类
 * </p>
 *
 * @author James Chen right_way@foxmail.com
 * @since 2020-10-16
 */
@Service
public class SysDepartmentServiceImpl extends ServiceImpl<SysDepartmentMapper, SysDepartment> implements ISysDepartmentService {

}
