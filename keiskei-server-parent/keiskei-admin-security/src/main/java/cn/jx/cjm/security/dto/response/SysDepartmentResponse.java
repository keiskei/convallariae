package cn.jx.cjm.security.dto.response;

import cn.jx.cjm.common.base.ParentEntity;
import cn.jx.cjm.common.util.LocalDateTimeDeserializer;
import cn.jx.cjm.common.util.LocalDateTimeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;


/**
 * <p>
 * 部门 admin 返回类
 * </p>
 *
 * @author James Chen right_way@foxmail.com
 * @since 2020-10-16
 */

@Data
@ApiModel(value = "SysDepartmentResponse", description = "部门 后台返回类")
public class SysDepartmentResponse implements ParentEntity<SysDepartmentResponse> {

    @ApiModelProperty(value = "部门id", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "上级部门", dataType = "Integer")
    private Integer parentId;


    @ApiModelProperty(value = "部门名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "子集", dataType = "List")
    private List<SysDepartmentResponse> children;

    @ApiModelProperty(value = "创建时间", dataType = "String")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间", dataType = "String")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime updateTime;
}
