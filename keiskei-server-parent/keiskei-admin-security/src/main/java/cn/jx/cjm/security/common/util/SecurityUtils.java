package cn.jx.cjm.security.common.util;

import cn.jx.cjm.common.enums.BizExceptionEnum;
import cn.jx.cjm.common.exception.BizException;
import cn.jx.cjm.security.common.bean.TokenUser;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtils {
    public static TokenUser getSessionUser() {
        TokenUser tokenUser = (TokenUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (null == tokenUser) {
            throw new BizException(BizExceptionEnum.AUTH_ERROR.getCode(), BizExceptionEnum.AUTH_ERROR.getMsg());
        }

        return tokenUser;
    }

}
