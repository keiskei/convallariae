package cn.jx.cjm.security.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 用户token相关数据
 *
 * @author 陈加敏 right_way@foxmail.com
 * @since 2019/8/10 13:59
 */
@Component
@ConfigurationProperties(prefix = "keiskei-server-conf.user-token")
@Data
public class UserTokenProperties {

    /**
     * 不进行拦截路径
     */
    private String permitUri = "";

    /**
     * 登陆后可访问的路径
     */
    private String authenticatedUrl = "";

    /**
     * 用户 token header名称
     */
    private String tokenHeader = "Auth-Token";
    /**
     * 用户默认密码
     */
    private String defaultPassword = "123456";
    /**
     * websocket header名称
     */
    private String websocketHeader = "Sec-WebSocket-Protocol";
    /**
     * 秘钥
     */
    private String secret = "secret";
    /**
     * 管理员登录URL
     */
    private String authWebLoginPath = "/admin/security/login";
    /**
     * 管理员退出URL
     */
    private String authWebLogoutPath = "/admin/security/logout";

    /**
     * 密码错误锁定分钟数
     */
    private Long lockMinutes = 10L;
}
