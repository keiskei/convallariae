package cn.jx.cjm.security.service;

import cn.jx.cjm.security.entity.SysPermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 后台权限详情信息 服务类
 * </p>
 *
 * @author 陈加敏 right_way@foxmail.com
 * @since 2019-06-24
 */
public interface ISysPermissionService extends IService<SysPermission> {


}
