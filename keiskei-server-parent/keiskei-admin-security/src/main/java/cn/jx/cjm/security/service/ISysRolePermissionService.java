package cn.jx.cjm.security.service;

import cn.jx.cjm.security.entity.SysRolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色-权限对应信息 服务类
 * </p>
 *
 * @author 陈加敏 right_way@foxmail.com
 * @since 2019-06-24
 */
public interface ISysRolePermissionService extends IService<SysRolePermission> {


}
