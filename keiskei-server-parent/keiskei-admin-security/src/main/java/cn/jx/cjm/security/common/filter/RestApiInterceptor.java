package cn.jx.cjm.security.common.filter;

import cn.jx.cjm.security.common.util.ResponseUtils;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author James Chen right_way@foxmail.com
 * @since 2020/9/25 12:29 下午
 */
@NoArgsConstructor
@AllArgsConstructor
public class RestApiInterceptor extends HandlerInterceptorAdapter {

    private ResponseUtils responseUtils;

    @Override
    public boolean preHandle(@NonNull HttpServletRequest request, @NonNull HttpServletResponse response, @NonNull Object handler) {
        responseUtils.confirm(request, response);

        return true;
    }
}
