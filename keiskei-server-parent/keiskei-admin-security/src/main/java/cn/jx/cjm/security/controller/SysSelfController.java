package cn.jx.cjm.security.controller;

import cn.jx.cjm.common.util.JwtTokenUtils;
import cn.jx.cjm.security.common.bean.TokenUser;
import cn.jx.cjm.security.common.util.SecurityUtils;
import cn.jx.cjm.security.dto.request.SysUserSelfRequest;
import cn.jx.cjm.security.dto.response.SysUserSelfResponse;
import cn.jx.cjm.security.support.SysUserSelfSupportService;
import com.baomidou.mybatisplus.extension.api.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 管理员信息controller
 *
 * @author 陈加敏
 * @since 2019年7月10日 下午5:35:44
 */
@Api(tags = "后台 - 管理员SELF")
@RestController
@RequestMapping("/admin/security/self")
public class SysSelfController {

    @Autowired
    private SysUserSelfSupportService sysUserSelfSupportService;


    @ApiOperation("刷新token")
    @GetMapping("/token")
    public R<String> refuseToken() {
        TokenUser tokenUser = SecurityUtils.getSessionUser();
        return R.ok(JwtTokenUtils.getJwtString(tokenUser));
    }

    @ApiOperation("获取当前信息")
    @GetMapping
    public R<SysUserSelfResponse> userInfo() {
        return R.ok(sysUserSelfSupportService.getCurrentSysUser());
    }


    @ApiOperation("更新自己信息")
    @PutMapping
    public R<Boolean> update(@RequestBody @Validated({Update.class}) SysUserSelfRequest request) {
        sysUserSelfSupportService.update(request);
        return R.ok(true);
    }

    @ApiOperation("更新管理员密码")
    @PatchMapping
    public R<Boolean> updatePassword(@RequestBody @Validated({UpdateProvider.class}) SysUserSelfRequest request) {
        sysUserSelfSupportService.updatePassword(request);
        return R.ok(true);
    }

}
