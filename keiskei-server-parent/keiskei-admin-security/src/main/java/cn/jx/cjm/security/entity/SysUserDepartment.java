package cn.jx.cjm.security.entity;

import cn.jx.cjm.common.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 管理员信息部门
 * </p>
 *
 * @author James Chen right_way@foxmail.com
 * @since 2020-10-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_user_department")
public class SysUserDepartment extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 管理员
     */
    private Integer sysUserId;

    /**
     * 所属部门
     */
    private Integer sysDepartmentId;


}
