package cn.jx.cjm.security.mapper;

import cn.jx.cjm.security.entity.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户-角色对应信息 Mapper 接口
 * </p>
 *
 * @author 陈加敏 right_way@foxmail.com
 * @since 2019-06-24
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
