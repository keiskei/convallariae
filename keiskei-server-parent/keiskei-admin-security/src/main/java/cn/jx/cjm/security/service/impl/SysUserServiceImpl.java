package cn.jx.cjm.security.service.impl;

import cn.jx.cjm.security.entity.SysUser;
import cn.jx.cjm.security.mapper.SysUserMapper;
import cn.jx.cjm.security.service.ISysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 管理员 服务实现类
 * </p>
 *
 * @author James Chen right_way@foxmail.com
 * @since 2020-10-16
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {

}
