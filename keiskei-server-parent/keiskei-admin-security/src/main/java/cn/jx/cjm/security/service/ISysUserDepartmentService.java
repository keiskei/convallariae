package cn.jx.cjm.security.service;

import cn.jx.cjm.security.entity.SysUserDepartment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 管理员信息部门 服务类
 * </p>
 *
 * @author James Chen right_way@foxmail.com
 * @since 2020-10-16
 */
public interface ISysUserDepartmentService extends IService<SysUserDepartment> {

}
