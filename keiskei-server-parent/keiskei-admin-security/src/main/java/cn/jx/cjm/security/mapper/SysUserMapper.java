package cn.jx.cjm.security.mapper;

import cn.jx.cjm.security.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 管理员 Mapper 接口
 * </p>
 *
 * @author James Chen right_way@foxmail.com
 * @since 2020-10-16
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
