package cn.jx.cjm.security.service.impl;

import cn.jx.cjm.security.entity.SysRolePermission;
import cn.jx.cjm.security.mapper.SysRolePermissionMapper;
import cn.jx.cjm.security.service.ISysRolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色-权限对应信息 服务实现类
 * </p>
 *
 * @author 陈加敏 right_way@foxmail.com
 * @since 2019-06-24
 */
@Service
public class SysRolePermissionServiceImpl extends ServiceImpl<SysRolePermissionMapper, SysRolePermission> implements ISysRolePermissionService {


}
