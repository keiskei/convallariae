package cn.jx.cjm.security.dto.request;

import cn.jx.cjm.common.util.LocalDateTimeDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 管理员 admin 接收类
 * </p>
 *
 * @author James Chen right_way@foxmail.com
 * @since 2020-10-16
 */


@Data
@ApiModel(value = "SysUserRequest", description = "管理员 后台接受类")
public class SysUserRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "管理员id", dataType = "Integer")
    @NotNull(message = "请选择管理员", groups = {Update.class, SelectKey.class})
    private Integer id;

    @ApiModelProperty(value = "登录用户名", dataType = "String")
    @NotBlank(message = "请输入登录用户名", groups = {Insert.class})
    private String username;

    @ApiModelProperty(value = "密码", dataType = "String")
    private String password;

    @ApiModelProperty(value = "真实姓名", dataType = "String")
    @NotBlank(message = "请输入真实姓名", groups = {Insert.class})
    private String realName;

    @ApiModelProperty(value = "账号过期时间", dataType = "LocalDateTime")
    @NotNull(message = "请选择账号过期时间", groups = {Insert.class})
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime accountExpiredTime;

    @ApiModelProperty(value = "状态", dataType = "Integer")
    @NotNull(message = "请选择状态", groups = {Insert.class})
    private Integer status;

    @ApiModelProperty(value = "头像", dataType = "String")
    private String avatar;

    @ApiModelProperty(value = "手机号", dataType = "String")
    @NotBlank(message = "请输入手机号", groups = {Insert.class})
    private String phone;

    @ApiModelProperty(value = "邮箱", dataType = "String")
    @NotBlank(message = "请输入邮箱", groups = {Insert.class})
    @Email(message = "邮箱格式错误", groups = {Insert.class, Update.class})
    private String email;

    @ApiModelProperty(value = "用户角色", dataType = "String[]")
    @NotEmpty(message = "请选择角色", groups = {Insert.class})
    private List<Integer> sysRoleIds;

    @ApiModelProperty(value = "用户部门", dataType = "String[]")
    @NotEmpty(message = "请选择角色", groups = {Insert.class})
    private List<Integer> sysDepartmentIds;




}
