package cn.jx.cjm.security.common.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

/**
 * 权限用户
 *
 * @author James Chen right_way@foxmail.com
 * @since 2018年10月11日 下午4:19:31
 */
@Data
public class TokenUser implements UserDetails {

    private static final long serialVersionUID = 3149333856334677639L;

    private Integer id;
    private String token;
    private String username;
    private String realName;
    private String avatar;
    private String phone;
    private String email;

    @JsonIgnore
    private String password;

    /**
     * 角色信息
     */
    private List<CjmGrantedAuthority> authorities;

    /**
     * 部门信息
     */
    private List<CjmDepartment> departments;

    /**
     * 账号是否可用
     */
    private boolean enabled = true;

    /**
     * 账号是否未过期
     */
    private boolean accountNonExpired = true;

    /**
     * 账号是否未被锁定
     */
    private boolean accountNonLocked = true;

    /**
     * 用户凭证是否为过期
     */
    private boolean credentialsNonExpired = true;


}
