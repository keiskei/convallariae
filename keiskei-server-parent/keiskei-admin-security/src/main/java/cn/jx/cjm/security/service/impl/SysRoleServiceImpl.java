package cn.jx.cjm.security.service.impl;

import cn.jx.cjm.security.mapper.SysRoleMapper;
import cn.jx.cjm.security.service.ISysRoleService;
import cn.jx.cjm.security.entity.SysRole;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * <p>
 * 角色信息 服务实现类
 * </p>
 *
 * @author 陈加敏 right_way@foxmail.com
 * @since 2019-06-24
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {
}
