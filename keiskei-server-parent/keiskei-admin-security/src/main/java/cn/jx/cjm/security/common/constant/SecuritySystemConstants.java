package cn.jx.cjm.security.common.constant;

/**
 * security 模块常量
 *
 * @author 陈加敏 right_way@foxmail.com
 * @since 2019/8/10 11:29
 */
public class SecuritySystemConstants {
    /**
     * 超级管理员id
     */
    public final static Integer SUPER_ADMIN_ID = -1;

    /**
     * options请求方式
     */
    public final static String OPTIONS = "OPTIONS";
}
