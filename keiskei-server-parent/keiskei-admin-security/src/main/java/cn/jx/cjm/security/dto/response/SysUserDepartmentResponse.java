package cn.jx.cjm.security.dto.response;

import cn.jx.cjm.common.util.LocalDateTimeDeserializer;
import cn.jx.cjm.common.util.LocalDateTimeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;


/**
 * <p>
 * 管理员信息部门 admin 返回类
 * </p>
 *
 * @author James Chen right_way@foxmail.com
 * @since 2020-10-16
 */

@Data
@ApiModel(value = "SysUserDepartmentResponse", description = "管理员信息部门 后台返回类")
public class SysUserDepartmentResponse {

    @ApiModelProperty(value = "管理员信息部门id", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "管理员", dataType = "Integer")
    private Integer sysUserId;

    @ApiModelProperty(value = "管理员", dataType = "String")
    private String sysUserName;


    @ApiModelProperty(value = "所属部门", dataType = "Integer")
    private Integer sysDepartmentId;

    @ApiModelProperty(value = "所属部门", dataType = "String")
    private String sysDepartmentName;


    @ApiModelProperty(value = "创建时间", dataType = "String")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间", dataType = "String")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime updateTime;
}
