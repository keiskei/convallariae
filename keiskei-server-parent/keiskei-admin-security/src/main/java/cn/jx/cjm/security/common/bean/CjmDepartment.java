package cn.jx.cjm.security.common.bean;

import lombok.Data;

import java.io.Serializable;

/**
 * @author James Chen right_way@foxmail.com
 * @since 2020/10/21 5:52 下午
 */
@Data
public class CjmDepartment implements Serializable {

    private static final long serialVersionUID = -8005062975450401769L;

    /**
     * 部门ID
     */
    private Integer departmentId;

    /**
     * 部门名称
     */
    private String departmentName;

    /**
     * 部门标识
     */
    private String departmentRule;
}
