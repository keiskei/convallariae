package cn.jx.cjm.thirdparty.cache.local;

import cn.jx.cjm.common.aop.annotation.LockerService;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>
 *
 * </p>
 *
 * @author ：陈加敏 right_way@foxmail.com
 * @since ：2019/12/13 2:05
 */
public class LocalLockerServiceImpl implements LockerService {

    /**
     * 公平锁
     */
    private final static Lock FAIR_LOCKER = new ReentrantLock(true);
    /**
     * 非公平锁
     */
    private final static Lock NONE_FAIR_LOCKER = new ReentrantLock(false);

    @Override
    public void lock(String lockKey) {
        NONE_FAIR_LOCKER.lock();
    }

    @Override
    public void unlock(String lockKey) {
        NONE_FAIR_LOCKER.unlock();
    }

    @Override
    public void lock(String lockKey, TimeUnit unit, int timeout) {
        try {
            NONE_FAIR_LOCKER.tryLock(timeout,unit);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean tryLock(String lockKey) {

        return NONE_FAIR_LOCKER.tryLock();

    }

    @Override
    public boolean tryLock(String lockKey, TimeUnit unit, long waitTime, long leaseTime) {
        try {
            return NONE_FAIR_LOCKER.tryLock(leaseTime,unit);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean fairLock(String lockKey, TimeUnit unit, long waitTime, long leaseTime) {
        try {
            return FAIR_LOCKER.tryLock(leaseTime,unit);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean isLocked(String lockKey) {
        return false;
    }
}
