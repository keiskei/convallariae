package cn.jx.cjm.thirdparty.cache.local.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author ：陈加敏 right_way@foxmail.com
 * @since ：2019/12/9 18:25
 */
@Data
@AllArgsConstructor
public class LocalCache implements Serializable {

    private static final long serialVersionUID = 3542624095794770764L;

    /**
     * 数据
     */
    private Object value;
    /**
     * 过期时间 毫秒
     */
    private Long expireTime;
}
