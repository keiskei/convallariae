package cn.jx.cjm.thirdparty.cache.local.util;


import java.io.*;

/**
 * 本地缓存工具类
 *
 * @author 陈加敏
 * @since 2019/7/13 16:45
 */
public class LocalCacheUtils {

    /**
     * 序列化 克隆处理
     * 将值通过序列化clone 处理后保存到缓存中，可以解决值引用的问题
     *
     * @param object .
     * @return .
     */
    @SuppressWarnings("unchecked")
	public static  <T extends Serializable> T clone(T object) {
        T cloneObject = null;
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(object);
            oos.close();
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            cloneObject = (T) ois.readObject();
            ois.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cloneObject;
    }

}
